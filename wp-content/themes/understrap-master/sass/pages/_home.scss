// ------ SLIDER -------- //
section#start-slider {
  position: relative;
}

.next_section {
  position: absolute;
  left: 50%;
  bottom: 60px;
  transform: translateX(-50%);
  display: block;
  width: rem(70);
  height: rem(70);
  border: 2px solid #fff;
  border-radius: 50%;
  z-index: 100;
  transition: $transition;

  .icon {
    color: #fff;
    line-height: 1;
    font-size: rem(26);
    @include centerAbs();
    height: 20px;
    transition: $transition;

    &:before {
      line-height: 1;
    }
  }

  &:hover {
    .icon {
      color: $green;
      top: 60%;
    }
  }
}

.products-names-slider {
  &.swiper-container-fade {
    .swiper-slide {
      .product-heading-box {
        opacity: 0;
        transition: $transition;
      }

      &.swiper-slide-active {
        .product-heading-box {
          opacity: 1;
        }
      }
    }
  }
}

.home_start_slider {
  height: calc(100vh - 152px);
  min-height: 550px;
  position: relative;
  overflow: hidden;

  .swiper-wrapper,
  .swiper-slide {
    height: calc(100vh - 152px);
    min-height: 550px;
    position: relative;
    background-size: cover;
    background-repeat: no-repeat;
  }

  .swiper-slide {
    overflow: hidden;

    &:before,
    &:after {
      content: '';
      position: absolute;
      top: 0;
      width: 25%;
      height: 100%;
      background: rgba(#0b2906, .4);
    }

    &:before {
      left: 0;
    }

    &:after {
      right: 0;
    }

    .white-center {
      width: 50%;
      height: 100%;
      background: rgba(#fff, .4);
      position: absolute;
      left: 25%;
      top: 0;

      .text-box {
        height: auto;
        width: 80%;
        top: 40%;
        left: 50%;
        transform: translate(-50%, -40%);
        position: absolute;

        .text {
          position: relative;
          height: 100%;
          width: 100%;
          border: 5px solid $white;

          p {
            width: 100%;
            height: 50%;
            padding: rem(65) 0 rem(75) 0;
            text-transform: uppercase;
            font-weight: 700;
            text-align: center;
            font-size: rem(68);
            line-height: rem(56);
            letter-spacing: -0.075em;
          }

          .learn-more {
            text-decoration: none;
            color: $dark-green;
            font-size: rem(12);
            font-weight: 600;
            text-transform: uppercase;
            padding: 16px;
            display: block;
            width: rem(210);
            position: absolute;
            left: 50%;
            bottom: 0;
            transform: translate(-50%, 50%);
            text-align: center;
            border: 5px solid $white;
            border-radius: rem(15);
            background-color: $yellow;
            transition: $transition;

            &:hover {
              background: $green;
              color: #fff;
            }
          }
        }
      }
    }
  }
}

.swiper-pagination {
  position: absolute;
  top: 75%;
  right: 0;
  transform: translate(0, -50%);

  .swiper-pagination-bullet {
    opacity: 1;
    background-color: transparent;
    margin-right: rem(15);

    .number {
      font-size: rem(16);
      font-weight: 500;
      color: $white;
    }
  }

  .swiper-pagination-bullet-active {
    font-weight: 900;
  }
}

// ------ ABOUT -------- //
#wrapper-about {
  min-height: 485px;
  position: relative;

  .about-text-box {
    padding: rem(45) 0 rem(85) 0;
    max-width: 630px;

    .section-heading {
      padding-bottom: rem(25);
    }

    p {
      font-weight: 300;
      font-size: rem(20);
      letter-spacing: -0.025em;
      color: $light-green;
      line-height: rem(28);
      margin-bottom: rem(43);
    }
  }

  .section-text {
    width: 50%;
    height: 100%;
    position: absolute;
    left: 0;
    top: 0;
    background: url('/wp-content/themes/understrap-master/assets/img/home-about-left.jpg');
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    z-index: -1;
  }

  .section-video {
    width: 50%;
    height: 100%;
    position: absolute;
    right: 0;
    top: 0;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    overflow: hidden;

    div {
      width: 100%;
      height: 100%;
      z-index: 2;
      background: rgba($dark-green, .56);
    }

    video {
      position: absolute;
      width: 100%;
      height: 100%;
      object-fit: cover;
      opacity: 0;
      transition: $transition;

      &.is-visible {
        opacity: 1;
      }
    }
  }
}

// ------ PRODUCT QUICK VIEWER -------- //
#wrapper-product-quick-viewer {
  min-height: 888px;
  background: radial-gradient(ellipse, #cfcfcf 100%, #fdfdfd 0%);

  .collapsing {
    //transition: 0.75s ease-in;
  }

  .section-heading {
    padding-top: rem(23);

    .light-blue {
      padding-left: rem(32);
    }
  }

  .home-quick-product-prev,
  .home-quick-product-next {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    z-index: 100;
    cursor: pointer;

    .icon {
      color: #000;
      font-size: 40px;
      transition: $transition;
    }

    &:hover {
      .icon {
        color: $green
      }
    }
  }

  .home-quick-product-prev {
    left: 0;
  }

  .home-quick-product-next {
    right: 0;
  }

  .categories-box {
    text-align: center;
    padding-top: rem(25);

    .categories {
      list-style: none;

      .product-category {
        display: inline-block;
        font-size: rem(20);
        line-height: rem(48);
        letter-spacing: -0.025em;
        font-weight: 500;
        color: $medium-grey;
        margin-right: rem(30);
        transition: $transition;

        &:hover { 
          color: $black;
        }

        &:last-child {
          margin-right: 0;
        }

        .choose-category {
          color: $light-blue;
        }

        .product-category-link {
          display: block;
        }
      }

      :not(.collapsed) {
        color: $black;
      }
    }
  }

  .swiper {
    .products-slider{
      padding-top: 80px;
      padding-bottom: 100px;
      img{
        &:hover{
          opacity: 0.7;
        }
      }
      &.swiper-is-loaded {
        .swiper-slide {
          transition: left 0.8s ease-in-out;
          img {
            transition: all 0.8s ease-in-out;
          }
        }
      }

      .swiper-wrapper {
        .swiper-slide {
          //transition: all 1s ease-in-out;
          position: relative;
          left: 0;

          img {
            //transition: all 1s ease-in-out;
            filter: blur(4px);
            max-width: 100%;
          }

          &.swiper-slide-prev {
            position: relative;
            left: -35px;
          }

          &.swiper-slide-next {
            position: relative;
            left: 35px;
          }

          &.swiper-slide-prev,
          &.swiper-slide-next,
          &.swiper-slide-duplicate-prev,
          &.swiper-slide-duplicate-next {
            img {
              filter: blur(1px);
              transform: scale(1.15);
            }
          }

          &.swiper-slide-active {
            img {
              filter: blur(0px);
              transform: scale(1.65);
            }
          }
        }
      }
    }
  }

  .product-heading-box {
    text-align: center;

    .product-heading {
      font-size: rem(36);
      font-weight: bold;
      letter-spacing: -0.025em;
      a{
        color: #000;
        transition: $transition;
        &:hover{
          color: $green;
        }
      }
    }
  }

  .button {
    padding: rem(50) 0;
    text-align: center;

  }
}

// ------ SYSTEM CALCULATOR -------- //
#wrapper-system-calculator {
  min-height: 444px;
  background-color: $green;
  position: relative;

  .system-calculator-text-box {
    padding: rem(62) rem(10) rem(85) rem(10);

    .section-heading {
      letter-spacing: -0.025em;
      margin-bottom: rem(17);
    }

    p {
      color: $darkest-green;
      font-size: rem(20);
      font-weight: 300;
      max-width: 540px;
      letter-spacing: -0.025em;
      line-height: rem(28);
      margin-bottom: rem(41);
    }
  }

  .right-img {
    position: absolute;
    right: 0%;
    top: -8%;
    height: 125%;
  }

  &::before {
    content: '';
    position: absolute;
    right: 0;
    top: 0;
    width: 30%;
    height: 100%;
    background: #cfcfcf;
  }
}

// ------ RENDER TEXTURES & COLOURS -------- //
#wrapper-render-textures-colours {
  min-height: 935px;
  background-repeat: no-repeat;
  background-position: left;
  background-size: 45%;
  position: relative;

  .render-textures-content {
    padding: rem(167) 0 0 rem(50);

    .section-heading {
      letter-spacing: -0.025em;
      font-size: rem(56);
    }

    .render-text {
      text-align: left;
      max-width: 725px;
      padding: rem(10) 0 rem(23) 0;

      p {
        font-size: rem(20);
        color: $dark-green;
        letter-spacing: -0.025em;
        line-height: rem(28);
        font-weight: 300;
      }
    }
  }

  .right-image {
    position: absolute;
    right: 0;
    bottom: 0;
    width: 41vw;
  }
}

// ------ WALLS BUILD'S UP -------- //
#wrapper-walls {
  min-height: 1040px;
  position: relative;

  .home_walls_slider {
    padding: rem(40) 0;
    position: relative;

    .walls-name {
      text-align: center;
    }
  }

  .home-walls-slider-prev,
    .home-walls-slider-next {
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      z-index: 100;
      cursor: pointer;
      outline: none;

      .icon {
        color: #fff;
        font-size: 30px;
        transition: $transition;
      }

      &:hover {
        .icon {
          color: $green
        }
      }
    }

    .home-walls-slider-prev {
      left: 0;
    }

    .home-walls-slider-next {
      right: 0;
    }

  .swiper-wrapper {
    h2 {
      color: #fff;
      font-size: rem(28);
    }
  }

  .section-heading {
    padding-top: rem(21);
    text-align: center;
    letter-spacing: -0.025em;
  }

  .walls-text {
    text-align: center;
    padding: rem(14) rem(105) 0 rem(155);

    p {
      font-size: rem(20);
      color: #b8beca;
      letter-spacing: -0.025em;
      line-height: rem(28);
      font-weight: 300;
    }
  }

  .left-content,
  .right-content {
    padding-top: rem(220);

    .content-item {
      font-size: rem(16);
      line-height: rem(28);
      font-weight: 300;
      color: #fefefe;
      letter-spacing: -0.025em;
      margin-bottom: 0;
      padding-bottom: 3px;
      cursor: pointer;
      display: block;
      position: relative;

      &:last-child {
        padding-bottom: 0;
      }

      &::after {
        content: '';
        position: absolute;
        width: 160%;
        height: 100%;
        background-color: #829bad;
        top: 0;
        z-index: -1;
        opacity: 0;
      }

      &:hover {
        &::after {
          opacity: 1;
        }
      }

      &.active {
        &::after {
          opacity: 1;
        }
      }
    }
  }

  .left-content {
    .content-item {
      &::after {
        left: 0;
      }
    }
  }

  .right-content {
    .content-item {
      &::after {
        right: 0;
      }
    }
  }

  .middle-content {
    position: relative;
    z-index: 1000;
    transition: $transition;

    &.image-loading {
      filter: blur(8px);
    }

    .image {
      max-height: 641px;
      max-width: 100%;
      opacity: 0;
      @include centerAbs();
      transition: $transition;

      &.first-image {
        position: relative;
        display: inline-block;
        left: 0;
        top: 0;
        transform: none;
      }

      &.active {
        opacity: 1;
      }
    }
  }

  .button {
    text-align: center;
  }
}

// ------ INFORMATIONS -------- //
#wrapper-informations {
  min-height: 690px;
  background: url('/wp-content/themes/understrap-master/assets/img/home-system-informations-background.png');
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;

  .section-heading {
    letter-spacing: -0.025em;
    text-align: center;
    padding-top: rem(33);
  }

  .links-wrapper {
    padding-top: rem(85);

    .link-content {
      a {
        text-decoration: none;
        max-height: 339px;
        color: $black;
        text-align: center;
        background-color: $light-grey;
        display: block;
        padding: rem(46) rem(20);
        transition: $transition;
        border-bottom: 0px solid #194213;

        img {
          height: 143px;
        }

        h5 {
          font-size: rem(20);
          line-height: rem(48);
          letter-spacing: -0.025em;
          text-transform: uppercase;
          padding-top: rem(12);

          &::after {
            content: '';
            display: block;
            width: 80%;
            border-bottom: 2px solid rgba(0, 0, 0, 0.2);
            position: absolute;
            left: 50%;
            transform: translateX(-50%);
          }
        }

        p {
          font-size: rem(14);
          line-height: rem(20);
          letter-spacing: -0.025em;
          font-weight: 300;
        }

        &:hover {
          background-color: #fff;
          box-shadow: 0px 0px 32px 0px rgba(2, 2, 2, 0.31);
          border-bottom: 10px solid #194213;
        }
      }
    }
  }
}

// ------ COMPANY NEWS -------- //
#company-news {
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  padding: rem(15) 0 rem(80) 0;

  .section-heading {
    letter-spacing: -0.025em;
  }
}

// ------ FOOTER -------- //
#wrapper-seo-links {
  background-image: url('/wp-content/themes/understrap-master/assets/img/footer-bg.png');
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  position: relative;

  &::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 30.33333333%;
    height: 100%;
    background: $light-grey;
  }

  .left-side {
    padding-top: rem(75);
  }

  .company-address {
    padding-top: rem(36);
    line-height: rem(28);
    letter-spacing: -0.025em;
    color: $darker-green;
    font-size: rem(16);

    &::after {
      content: '';
      display: block;
      width: 80%;
      border-bottom: 2px solid rgba(0, 0, 0, 0.2);
      padding-top: rem(26);
    }
  }

  .social {
    padding-top: rem(14);

    .social-heading {
      color: $darker-green;
      letter-spacing: -0.025em;
      font-size: rem(16);
      font-weight: bold;
    }

    .social-links {
      font-size: rem(45);

      a {
        margin-right: 10px;
        transition: $transition;

        &:last-child {
          margin-right: 0;
        }

        &:hover {
          opacity: 0.7;
          transform: rotate(15deg);
        }
      }
    }
  }

  .seo-category {
    padding: rem(40) 0 rem(10) 0;
  }

  .category-name {
    text-transform: uppercase;
    font-size: rem(16);
    font-weight: bold;
    letter-spacing: -0.025em;
    color: $darker-green;

    &::before {
      content: '';
      display: inline-block;
      height: rem(67);
      border-left: 1px solid #60a557;
      margin-left: rem(-14);
      width: 14px;
    }
  }

  ul {
    list-style-type: none;
    padding: 0;

    li {
      line-height: rem(28);

      &:first-child {
        padding-top: rem(14);
      }

      &::before {
        content: '\0045';
        font-family: 'icons';
        font-size: .75rem;
        color: $darker-green;
      }

      a {
        font-size: rem(16);
        text-decoration: none;
        color: $light-green-2;
        letter-spacing: -0.025em;
        transition: $transition;

        &:hover {
          color: $white;
        }
      }
    }
  }

  .copyright-box {
    background-color: #194213;

    .copyright-text {
      font-size: rem(10);
      line-height: 32px;
      text-transform: uppercase;
      margin-bottom: 0;
      letter-spacing: -0.025em;
      color: #6e9c68;
    }
  }
}

// MEDIA QUERIES

@include media-breakpoint-down(xl) {
  #wrapper-system-calculator {
    min-height: 0;

    .system-calculator-text-box {
      padding: rem(40) 0;

      p {}

      a {}
    }

    .right-img {
      top: -3%;
      right: -2%;
      height: 110%;
    }
  }

  #wrapper-render-textures-colours {
    min-height: 0;
    padding: rem(40) 0;

    .render-textures-content {
      padding: 0 0 rem(200) rem(50);
    }
  }

  #wrapper-product-quick-viewer {
    min-height: 0;

    .section-heading {
      padding-top: rem(40);
    }

    .button {
      padding: rem(40) 0 rem(40) 0;
    }
  }

  #wrapper-walls {
    min-height: 0;

    .section-heading {
      padding-top: rem(40);
    }

    .button {
      padding-bottom: rem(40);
    }
  }

  #wrapper-informations {
    min-height: 0;

    .section-heading {
      padding-top: rem(40);
    }

    .links-wrapper {
      padding: rem(40) 0;
    }
  }
}

@include media-breakpoint-down(lg) {
  #wrapper-system-calculator {
    text-align: center;

    .system-calculator-text-box {
      padding-top: rem(40);
      padding-bottom: 0;

      p {
        display: inline-block;
        padding-bottom: rem(40);
        margin-bottom: 0;
      }

      a {
        margin-bottom: rem(40);
      }
    }

    .right-img {
      position: relative;
      height: auto;
      width: 70%;
      right: 0;
      top: 0;
      padding-bottom: rem(40);
    }

    &::before {
      display: none;
    }
  }

  #wrapper-render-textures-colours {
    .right-image {
      width: 35vw;
    }
  }

  #wrapper-seo-links {
    ul {
      li {
        a {
          font-size: rem(15);
        }
      }
    }
  }
}

@include media-breakpoint-down(md) {
  #wrapper-product-quick-viewer{
    .swiper{
      .products-slider{
        .swiper-wrapper{
          .swiper-slide{
            &.swiper-slide-next{
              left: 0;
              img{
                transform: scale(0.8);
              }
            }
            &.swiper-slide-prev{
              left: 0;
              img{
                transform: scale(0.8);
              }
            }
            &.swiper-slide-active{
              img{
                transform: scale(1.2);
              }
            }
          }
        }
      }
    }
    .product-heading-box .product-heading{
      font-size: 24px;
    }
  }

  .home_aside {
    display: none;
  }

  .home_start_slider {
    .swiper-slide {
      .white-center {
        .text-box {
          .text {
            border: none;
          }
        }
      }
    }
  }

  #wrapper-about {
    min-height: 0;

    .about-text-box {
      padding: rem(40) 0;
      text-align: center;
    }

    .section-video {
      display: none;
    }

    .section-text {
      width: 100%;
    }
  }

  #wrapper-product-quick-viewer {
    .swiper {
      .products-slider {
        padding: rem(40) 0;
      }
    }

    .categories-box {
      .categories {
        padding: 0;

        .product-category {
          margin-right: rem(15);
        }
      }
    }
  }

  #wrapper-render-textures-colours {
    background-size: 40%;

    .right-image {
      display: none;
    }

    .render-textures-content {
      padding: 0;
    }
  }

  #wrapper-walls {

    .left-content,
    .right-content {
      padding-top: rem(50);

      .content-item::after {
        width: 100%;
      }
    }

    .walls-text {
      padding: rem(10) rem(30);
    }

    .button {
      padding-top: rem(40);
    }
  }

  #wrapper-informations {
    .links-wrapper {
      .link-content {
        margin-bottom: rem(40);

        a {
          min-height: 0;

          img {
            height: rem(100);
          }

          p {
            font-size: rem(16);
          }
        }
      }
    }
  }

  #wrapper-seo-links {
    overflow: hidden;
    
    .container {
      padding: 0 15px;
    }

    &::before {
      display: none;
    }

    .left-side {
      padding-top: rem(10);
      position: relative;

      &::after {
        content: '';
        position: absolute;
        top: 0;
        left: -50%;
        width: 200%;
        height: 100%;
        background-color: #fff;
      }
    }

    .company-address,
    .social,
    .logo {
      z-index: 999;
      padding: rem(40) 0;
    }

    .social {
      .social-heading {
        margin-bottom: rem(15);
      }
    }

    .company-address {
      margin: 0;

      &::after {
        display: none;
      }
    }

    .copyright-box {
      text-align: center;
    }
  }
}

@include media-breakpoint-down(sm) {
  .home_start_slider {
    .swiper-slide {

      &::before,
      &::after {
        display: none;
      }

      .white-center {
        left: 0;
        width: 100%;

        .text-box {
          .text {
            p {
              font-size: rem(48);
            }
          }
        }
      }
    }
  }

  #wrapper-render-textures-colours {
    .render-textures-content {
      .section-heading {
        font-size: rem(42);
      }
    }
  }

  #wrapper-walls {
    .middle-content {
      .image {
        max-height: 300px;
      }
    }

    .left-content,
    .right-content {
      .content-item {
        padding: 0 rem(15);
      }
    }

    .walls-text {
      padding: rem(10) 0;
    }
  }

  #wrapper-seo-links {
    .seo-category {
      padding: rem(40) 0;

      &.no-padding-top {
        padding-top: 0;
      }

      .category-name {
        &::before {
          display: none;
        }
      }
    }

    ul {
      margin-bottom: 0;
    }

    .logo {
      width: 25%;
    }

    .social {
      .social-links {
        font-size: rem(38);

        a {
          margin-right: 0;
        }
      }
    }
  }
}

@include media-breakpoint-down(xs) {

  #wrapper-product-quick-viewer{
    .swiper{
      .products-slider{
        padding: 0;
        .swiper-wrapper{
          .swiper-slide{
            &.swiper-slide-next{
              left: 0;
              img{
                transform: scale(0.8);
              }
            }
            &.swiper-slide-prev{
              left: 0;
              img{
                transform: scale(0.8);
              }
            }
            &.swiper-slide-active{
              img{
                transform: scale(0.8);
              }
            }
          }
        }
      }
    }
    .product-heading-box .product-heading{
      font-size: 24px;
    }
  }
  .next_section {
    width: rem(50);
    height: rem(50);
    bottom: 60px;
  }

  .home_start_slider {
    min-height: calc(100vh - 20px);
    position: relative;
    overflow: hidden;

    .swiper-wrapper,
    .swiper-slide {
      min-height: calc(100vh - 20px);
      position: relative;
    }
  }

  #wrapper-render-textures-colours {
    padding: 0;
    background-size: cover;

    .render-textures-content {
      text-align: center;
      padding: rem(40) 0;
      .section-heading {
        font-size: rem(36) !important;
      }
      .render-text {
        text-align: center;
      }
    }

    .container {
      background-color: rgba(255, 255, 255, 0.85);
    }
  }

  #wrapper-product-quick-viewer {
    .section-heading {
      .light-blue {
        padding-left: 0;
      }
    }
  }

  #wrapper-walls {

    .right-content,
    .left-content {
      padding-top: rem(20);
    }
  }

  #wrapper-informations {
    .links-wrapper {
      .link-content {
        a {
          img {
            height: rem(75);
          }
        }
      }
    }
  }

  #wrapper-seo-links {
    .left-side {
      background-color: $light-grey;
      text-align: center;

      &::after {
        display: none;
      }
    }

    .seo-category {
      padding-left: 10px;
    }

    .company-address,
    .social,
    .logo {
      z-index: 999;
      padding: 0;
    }

    .logo {
      display: inline-block;
      padding-top: rem(30);
      padding-bottom: 0;
      width: 50%;
    }

    .company-address {
      padding-top: rem(30);
      padding-bottom: 0;
    }

    .social {
      padding: rem(30) 0;

      .social-links {
        a {
          margin-right: 15px;
        }
      }
    }
  }
  #company-news{
    padding-top:rem(50);
    padding-bottom:rem(50);
  }
  .company-news-number{
    display: none;
  }
  
}

@media all and (max-width: 349.98px) {
  #wrapper-seo-links {
    ul {
      li {
        a {
          font-size: rem(13);
        }
      }
    }
  }
}
