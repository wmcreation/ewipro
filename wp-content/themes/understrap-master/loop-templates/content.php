<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

?>

<article <?php post_class('col-md-6 col-lg-4 post_item'); ?> id="post-<?php the_ID(); ?>">
  <a href="<?php echo the_permalink($post->ID)?>">
    <div class="post_item_date">
      <span class="icon icon-calendar"></span>
      <span>
        <?php echo get_the_date('d.m.Y') ?>
      </span>
    </div>

    <div class="position-relative">
      <header class="entry-header">


        <!--		--><?php //the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
          //		'</a></h2>' ); ?>


      </header><!-- .entry-header -->

        <?php echo get_the_post_thumbnail( $post->ID, 'blog_thumb' ); ?>

      <div class="entry-content">

         <p><?php echo get_the_title($post->ID); ?></p>

          <?php
          wp_link_pages( array(
              'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
              'after'  => '</div>',
          ) );
          ?>

      </div><!-- .entry-content -->


    </div>

    <footer class="entry-footer">

      <span class="blog_item_link">READ ARTICLE</span>

    </footer><!-- .entry-footer -->
  </a>
</article><!-- #post-## -->
