<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

?>
<article <?php post_class('single_post_content os-animation'); ?> id="post-<?php the_ID(); ?>" data-os-animation="fadeInUp" data-os-animation-delay="0.4s">

	<header class="entry-header">

		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

    <div class="post_item_date">
      <span class="icon icon-calendar"></span>
      <span>
        <?php echo get_the_date('d.m.Y') ?>
      </span>
    </div>


	</header><!-- .entry-header -->

	<div class="entry-content">

		<?php the_content(); ?>

		<?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
			'after'  => '</div>',
		) );
		?>

	</div><!-- .entry-content -->

	<footer class="entry-footer">
    <div class="product_share">
      <div class="row">
        <div class="col-md-5">
          <div class="row">
            <div class="col-auto align-self-center">
              <h5 class="os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">Share</h5>
            </div>
            <div class="col-auto os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
              <a href="http://www.facebook.com/sharer.php?u=<?php esc_url(the_permalink());?>" target="_blank" class="social-link-fb"><i class="icon icon-fb"></i></a>
              <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&source=<?php echo esc_url(home_url()); ?>"  count-layout="none" target="_blank" class="social-link-in"><i class="icon icon-in"></i></a>
              <a href="http://twitter.com/home?status=Reading: <?php esc_url(the_permalink()); ?>" title="<?php esc_html_e('','ewipro'); ?>" target="_blank" class="social-link-tw"><i class="icon icon-tw"></i></a>
            </div>
          </div>
        </div>
        <div class="col-md-7 text-center text-md-right">
            <?php
            $prev_post = get_previous_post();
            if (!empty( $prev_post )): ?>
              <a class="read-more-button prev" href="<?php echo esc_url( get_permalink( $prev_post->ID ) ); ?>">PREVIOUS</a>
            <?php endif; ?>
            <?php
            $next_post = get_next_post();
            if (!empty( $next_post )): ?>
              <a class="read-more-button next" href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>">NEXT</a>
            <?php endif; ?>
        </div>
      </div>
    </div>
	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
