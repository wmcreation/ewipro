<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

get_header();
$container   = get_theme_mod( 'understrap_container_type' );

?>



  <div class="wrapper" id="single-wrapper">
    <div class="header-wrapper blog-header">
      <div class="container">
        <div class="row">
          <div class="col-12 col-lg-5 extra-wrapper">
            <h2 class="section-heading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">
              <span class="section-number"><span class="resources-number">01</span></span>
              <span class="white">
                <span>Company</span> news
              </span>
            </h2>
          </div>
          <div class="col-12 col-lg-5 extra-wrapper align-self-end os-animation" data-os-animation="fadeInRight" data-os-animation-delay="0.3s">
            <p>Keep up to date with the latest company news, product information and technical advice.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

      <div class="row">
        
        <main class="site-main col-lg-7 main_post_col" id="main">

            <?php while ( have_posts() ) : the_post(); ?>

                <?php get_template_part( 'loop-templates/content', 'single' ); ?>



<!--                --><?php
//                // If comments are open or we have at least one comment, load up the comment template.
//                if ( comments_open() || get_comments_number() ) :
//                    comments_template();
//                endif;
//                ?>

            <?php endwhile; // end of the loop. ?>

        </main><!-- #main -->

        <!-- Right sidebar -->
        <aside class="col-xl-4 col-lg-5 offset-xl-1 sidebar_post_parent">
          <?php get_sidebar('blog_content'); ?>
        </aside>

      </div><!-- .row -->

    </div><!-- Container end -->

  </div><!-- Wrapper end -->



<?php get_footer(); ?>
