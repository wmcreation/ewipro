<?php
/**
 * Template Name: Home product quick viewer
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

$current_post_id = $post->ID;

$args = array(
  'post_type'      => 'page',
  'posts_per_page' => -1,
  'post_parent'    => $post->ID,
  'order'          => 'ASC',
  'orderby'        => 'menu_order'
);

$args_cat = array(
  'taxonomy' => 'products_tax',
  'hide_empty' => false
);

$categories = get_terms($args_cat);
$charsToReplace = [' ', "&", 'amp;'];
$i = 0;
$j = 0;

?>
<?php if(!get_field('section_on', $post->ID)): ?>
  <section id="<?php echo $post->post_name ?>">
    <div id="wrapper-product-quick-viewer">
      <div class="container">
        <div class="row">
          <div class="col">
            <h2 class="section-heading text-center os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">
              <span class="section-number"><span class="product-number">02</span></span>
              <span class="light-blue"><?php echo the_title() ?></span>
            </h2>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col">
            <div class="categories-box os-animation" data-os-animation="fadeInRight" data-os-animation-delay="0.3s">
              <ul class="categories">
                <li class="product-category d-none d-md-inline-block"><span class="choose-category">Choose category</span></li>
                <?php foreach ($categories as $term): ?>

                  <?php
                    $args_posts = array(
                      'post_type' => 'ewi_product',
                      'posts_per_page' => -1,
                      'tax_query' => array(
                        array(
                          'taxonomy' => 'products_tax',
                          'field' => $term->term_id,
                          'terms' => $term->name,
                        )
                      ),
                    );
                    $posts = get_posts($args_posts);
                  ?>
                  <li class="product-category">
                    <a class="product-category product-category-link  <?php if( $i != 0 ) echo 'collapsed'; ?>" data-toggle="collapse" href="#"
                    data-target="#<?php echo str_replace($charsToReplace, '', $term->name); ?>Slider" aria-expanded="false"
                    aria-controls="<?php echo str_replace($charsToReplace, '', $term->name); ?>Slider">
                      <?php echo $term->name; ?>
                    </a>
                    <?php $i++; ?>
                  </li>
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
        </div>
      </div>

       <div class="container os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
        <div class="row">
          <div class="col" id="products-collapse">
            <?php foreach($categories as $term): ?>

              <?php
                $args_posts = array(
                  'post_type' => 'ewi_product',
                  'posts_per_page' => -1,
                  'tax_query' => array(
                    array(
                      'taxonomy' => 'products_tax',
                      'field' => $term->term_id,
                      'terms' => $term->name,
                    )
                  ),
                );

                $posts = get_posts($args_posts);
              ?>

              <div class="collapse <?php if( $j< 1 ) echo 'show'; ?>" id="<?php echo str_replace($charsToReplace, '', $term->name); ?>Slider" data-parent="#products-collapse">
                <?php $j++; ?>
                <div class="swiper">
                  <div class="swiper-container products-slider">
                    <div class="swiper-wrapper">
                      <?php foreach($posts as $key => $value): ?>
                        <?php $current_term_name = wp_get_post_terms($value->ID, 'products_tax')[0]->name; ?>
                        <?php if( $current_term_name === $term->name): ?>
                          <div class="swiper-slide">
                            <a href="<?php echo get_permalink($value->ID) ?>">
                              <img id="img_<?php echo ($value->ID); ?>" data-src="<?php echo get_the_post_thumbnail_url($value->ID, 'product-thumb'); ?>"
                                   alt="<?php echo $value->post_title; ?> image" class="swiper-lazy">
                            </a>
                          </div>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </div>
                    <div class="home-quick-product-prev"><span class="icon icon-arrow-left"></span></div>
                    <div class="home-quick-product-next"><span class="icon icon-arrow-right"></span></div>
                  </div>
                </div>

                <div class="swiper">
                  <div class="swiper-container products-names-slider">
                    <div class="swiper-wrapper">
                    <?php foreach($posts as $key => $value): ?>
                        <?php $current_term_name = wp_get_post_terms($value->ID, 'products_tax')[0]->name; ?>
                        <?php if( $current_term_name === $term->name): ?>
                          <div class="swiper-slide">
                            <div class="product-heading-box">
                              <h2 class="product-heading">
                                <a href="<?php echo get_permalink($value->ID) ?>">
                                  <?php echo $value->post_title; ?>
                                </a>
                              </h2>
                            </div>
                          </div>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </div>
                  </div>
                </div>

              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col">
            <div class="button">
              <a href="/products-viewer/" class="read-more-button">view all products</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>