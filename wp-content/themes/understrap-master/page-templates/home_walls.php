<?php
/**
 * Template Name: Home walls section
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */
$args = array(
  'post_type'      => 'page',
  'posts_per_page' => -1,
  'post_parent'    => $post->ID,
  'order'          => 'ASC',
  'orderby'        => 'menu_order'
);

$args_post = array(
  'post_type'       => 'walls',
  'posts_per_page'  => 100
);

$args_terms = array(
  'taxonomy' => 'walls_tax',
  'hide_empty' => false
);

$posts = get_posts($args_post);
$posts_length = count($posts);
$posts_firsthalf = array_slice($posts, 0, $posts_length/2);
$posts_secondhalf = array_slice($posts, $posts_length/2);
$terms = get_terms($args_terms);
?>
<?php if(!get_field('section_on', $post->ID)): ?>
  <section id="<?php echo $post->post_name ?>">
    <div id="wrapper-walls" style="background-image: url(<?php echo get_the_post_thumbnail_url() ?>)">

    <?php if(wp_is_mobile()): ?> <!-- MOBILES -->
      <div class="container">

      <div class="row">

          <div class="col-12">
            <h2 class="section-heading os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
              <span class="section-number"><span class="walls-number">05</span></span>
              <span class="white">
                  <?php echo the_title() ?>
              </span>
            </h2>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="walls-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
              <?php echo the_content() ?>
            </div>
          </div>

        </div>


        <div class="row">
          <div class="col-12">
            <div class="swiper">
              <div class="swiper-container home_walls_slider">
                <div class="swiper-wrapper">
                  <?php foreach($posts as $key => $value): ?>
                    <div class="swiper-slide">
                      <a href="<?php echo get_post_permalink($value->ID); ?>">
                        <img id="img_<?php echo ($value->ID); ?>" data-src="<?php echo get_the_post_thumbnail_url($value->ID); ?>"
                        alt="<?php echo $value->post_title; ?> image" class="swiper-lazy">
                      </a>
                      <h2 class="walls-name"><?php echo $value->post_title; ?></h2>
                    </div>
                  <?php endforeach; ?>
                </div>
              </div>
              <div class="home-walls-slider-prev"><span class="icon icon-arrow-left"></span></div>
              <div class="home-walls-slider-next"><span class="icon icon-arrow-right"></span></div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col">
            <div class="button">
              <a href="/system-build-ups/" class="read-more-button">view all build-ups</a>
            </div>
          </div>
        </div>

      </div>

      <?php else: ?> <!-- DESKTOPS -->
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h2 class="section-heading os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
              <span class="section-number"><span class="walls-number">05</span></span>
              <span class="white">
                  <?php echo the_title() ?>
              </span>
            </h2>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="walls-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
              <?php echo the_content() ?>
            </div>
          </div>
        </div>

        <div class="row no-gutters">

          <div class="col-12 col-sm-6 order-1 col-md-4 order-md-1 col-lg-3 text-right">
            <div class="left-content os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.6s">
              <?php $i = 0; foreach($posts_firsthalf as $key => $value): ?>
                <a class="content-item <?php if($i == 0) echo 'active' ?>" data-id="img_<?php echo ($value->ID) ?>"><?php echo $value->post_title ?></a>
              <?php $i++; endforeach; ?>
            </div>
          </div>

          <div class="col-12 order-2 col-md-4 order-md-2 col-lg-6 text-center middle-content os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.6s">

              <?php $i = 0; foreach($posts as $key => $value): ?>
                  <?php if($i == 0): ?>
                    <a href="<?php echo get_post_permalink($value->ID); ?>">
                      <img id="img_<?php echo ($value->ID) ?>" src="<?php echo get_the_post_thumbnail_url($value->ID) ?>" data-src="<?php echo get_the_post_thumbnail_url($value->ID) ?>" alt="logo" class="first-image active image">
                    </a>
                  <?php else: ?>
                    <a href="<?php echo get_post_permalink($value->ID); ?>">
                      <img id="img_<?php echo ($value->ID) ?>" data-src="<?php echo get_the_post_thumbnail_url($value->ID) ?>" alt="logo" class="image">
                    </a>
                  <?php endif; ?>
              <?php $i++; endforeach; ?>

          </div>

          <div class="col-12 col-sm-6 order-1 offset-md-0 col-md-4 order-md-3 col-lg-3 text-left">
            <div class="right-content os-animation" data-os-animation="fadeInRight" data-os-animation-delay="0.6s">
              <?php foreach($posts_secondhalf as $key => $value): ?>
                <a class="content-item" data-id="img_<?php echo ($value->ID) ?>"><?php echo $value->post_title ?></a>
              <?php endforeach; ?>
            </div>
          </div>

        </div>

        <div class="row">
          <div class="col">
            <div class="button">
              <a href="/system-build-ups/" class="read-more-button">view all build-ups</a>
            </div>
          </div>
        </div>

      </div>

    <?php endif; ?>

      <!-- <div class="section-text">
        <div></div>
      </div>
      <div class="section-video" style="background-image: url(<?php //echo get_the_post_thumbnail_url($posts->ID,'full-hd') ?>)">
        <div></div>
      </div> -->
    </div>
  </section>
<?php endif; ?>
