<?php
/**
 * Template Name: Home about section
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */
$args = array(
  'post_type'      => 'page',
  'posts_per_page' => -1,
  'post_parent'    => $post->ID,
  'order'          => 'ASC',
  'orderby'        => 'menu_order'
); 
?>
<?php if(!get_field('section_on', $post->ID)): ?>
  <section id="<?php echo $post->post_name ?>">
    <div id="wrapper-about">
      <div class="container">
        <div class="row">
          <div class="col-12 col-lg-6">
            <div class="about-text-box">
              <h2 class="section-heading os-animation"  data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">
                <span class="section-number"><span class="about-number">01</span></span>
                <span class="white">
                  <?php echo get_post_meta($post->ID, 'about-heading-white')[0]; ?>
                </span>
                <span class="green">
                  <?php echo get_post_meta($post->ID, 'about-heading-green')[0]; ?>
                </span>
              </h2>
              <div class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s"><?php echo the_content() ?></div>
              <div class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.6s"><a href="/about-us/" class="read-more-button">Learn more</a></div>
            </div>
          </div>
        </div>
      </div>

      <div class="section-text">
        <div></div>
      </div>
      <div class="section-video" style="background-image: url(<?php echo get_the_post_thumbnail_url($post->ID, 'full-hd') ?>)">
        <video loop="" id="video-about-home" muted="" plays-inline="" poster="<?php echo get_the_post_thumbnail_url($post->ID,'full-hd') ?>">
          <source src="<?php echo get_template_directory_uri().'/src/video/web-about-us-2.mp4' ?>" type="video/mp4">
        </video>
        <div></div>
      </div>
    </div>
  </section>
<?php endif; ?>
