<?php
  /**
   * Template Name: System build ups
   *
   * Template for displaying a page just with the header and footer area and a "naked" content area in between.
   * Good for landingpages and other types of pages where you want to add a lot of custom markup.
   *
   * @package understrap
   */

  $args = array(
    'post_type' => 'page',
    'posts_per_page' => -1,
    'post_parent' => $post->ID,
  );

  $args_terms = array(
    'taxonomy' => 'walls_tax',
    'hide_empty' => false
  );

  $args_posts = array(
    'post_type' => 'walls',
    'posts_per_page' => -1
  );

  $terms = get_terms($args_terms); 
  $posts = get_posts($args_posts);

  get_header();
  ?>

  <div class="system_build_ups_wrapper">
    <div class="container">

      <div class="row heading_wrapper">
        <div class="col">
          <h2 class="section-heading">
            <span class="light-blue"><?php echo get_the_title($post->ID); ?></span>
          </h2>
        </div>
      </div>

      <div class="row text_wrapper pb-5">
        <div class="col">
          <p><?php echo the_content($post->ID); ?></p>
        </div>
      </div>

      <div class="row systems_wrapper">
        <?php foreach($terms as $term): ?>
          <?php foreach($posts as $key => $post): ?>
            <?php if( wp_get_post_terms($post->ID, 'walls_tax')[0]->name === $term->name ): ?>
              <div class="col-lg-4 col-sm-6">
                <div class="image_wrapper">
                  <a href="<?php echo get_permalink($post->ID); ?>" >
                    <img src="<?php echo get_the_post_thumbnail_url($post->ID, 'large'); ?>">
                    <h3 class="image_caption"><?php echo get_the_title($post->ID); ?></h3>
                    <span href="<?php echo get_permalink($post->ID); ?>" class="read-more-button system_link">Learn more</span>
                  </a>
                </div>
              </div>
            <?php endif; ?>
          <?php endforeach; ?>
        <?php endforeach; ?>
      </div>

    </div>
  </div>

  <?php
  get_footer();