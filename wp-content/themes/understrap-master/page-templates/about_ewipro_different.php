<?php
/**
 * Template Name: About What Makes EwiPro different
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */
$args = array(
  'post_type'      => 'page',
  'posts_per_page' => -1,
  'post_parent'    => $post->ID,
  'order'          => 'ASC',
  'orderby'        => 'menu_order'
); 

?>

<section id="<?php echo $post->post_name ?>">
  <div class="wrapper-what-makes-ewipro-different">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-6 col-xl-5 left-side-text">
          <h2 class="small-heading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s"><?php echo the_title() ?></h2>
          <div class="content-text"><?php echo the_content() ?></div>
        </div>
        <div class="col-4 offset-0 col-md-2 offset-xl-1 os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
          <img src="/wp-content/themes/understrap-master/assets/img/about_telephone.svg" alt="about telephone svg" class="contact-image">
        </div>
        <div class="col-8 col-md-4 right-side-text">
          <h2 class="contact-number os-animation" data-os-animation="fadeInRight" data-os-animation-delay="0.3s">
            <a href="tel:<?php echo get_post_meta($post->ID, 'about_contact_number')[0]; ?>"><?php echo get_post_meta($post->ID, 'about_contact_number')[0]; ?></a>
          </h2>
          <p class="content-text"><span class="black os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.6s"><?php echo get_post_meta($post->ID, 'about_availability')[0]; ?></span></p>
          <div class="button os-animation" data-os-animation="fadeInUp" data-os-animation-delay="animation-delay: 0.6s;">
            <a href="/contact/" class="read-more-button">Go to contact</a>
          </div>
        </div>
      </div>
    </div>
    <div class="right-side"></div>
    <div class="video-background section-video lazy" data-src="<?php echo get_the_post_thumbnail_url($post->ID, 'full-hd') ?>">
      <video loop="" id="video-about" muted="" plays-inline="" poster="<?php echo get_the_post_thumbnail_url($post->ID,'full-hd') ?>">
        <source src="<?php echo get_field('about-us-video', wp_get_post_parent_id( $post->ID )); ?>" type="video/mp4">
      </video>
    </div>
  </div>
</section>