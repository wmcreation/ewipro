<?php
  /**
   * Template Name: Text Sites
   *
   * Template for displaying a page just with the header and footer area and a "naked" content area in between.
   * Good for landingpages and other types of pages where you want to add a lot of custom markup.
   *
   * @package understrap
   */

  get_header();

  $first_content_subheading = get_field('first_content_subheading', $post->ID);
  $first_content_text = get_field('first_content_text', $post->ID);
  $second_content_subheading = get_field('second_content_subheading', $post->ID);
  $second_content_text = get_field('second_content_text', $post->ID);
  $content_list_heading = get_field('content_list_heading', $post->ID);
  $content_list = get_field('content_list', $post->ID);
  $third_content_subheading = get_field('third_content_subheading', $post->ID);
  $third_content_text = get_field('third_content_text', $post->ID);
  ?>

  <div class="text_site_wrapper">

    <div class="header-wrapper" style="background-image: url(<?php echo get_field('header_left_background', $post->ID); ?>)">
      <div class="container">

        <div class="row">
          <div class="col-12 col-lg-6">
            <h2 class="section-heading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">
              <span class="section-number"><span class="resources-number"><?php echo get_field('header_heading_number', $post->ID); ?></span></span>
              <span class="white">
                <?php echo get_field('header_heading', $post->ID); ?>
              </span>
            </h2>
          </div>
        </div>

        <div class="row">
          <div class="col-12 col-lg-5">
            <p class="resources-header-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
              <?php echo get_field('header_text', $post->ID) ?>
            </p>
          </div>
        </div>
      </div>
      <div class="d-none d-lg-block lazy right-side-background" data-src="<?php echo get_the_post_thumbnail_url($post->ID, 'full-hd'); ?>"></div>
    </div>

    <div class="content-wrapper">
      <div class="container">
      
      <?php if( $first_content_text || $first_content_subheading ): ?>

        <?php if( $first_content_subheading ): ?>

          <div class="row">
            <div class="col-12 col-lg-7">
              <h3 class="resources-subheading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.6s">
                <?php echo $first_content_subheading; ?>
              </h3>
              <div class="resources-section-text resources-content-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                <p><?php echo $first_content_text; ?></p>
              </div>
            </div>
          </div>

        <?php else: ?>

          <div class="row">
            <div class="col-12 col-lg-7">
              <div class="resources-section-text resources-content-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                <p><?php echo $first_content_text; ?></p>
              </div>
            </div>
          </div>

        <?php endif; ?>        

      <?php endif; ?>


      <?php if( $content_list || $content_list_heading ): ?>
      
        <?php if( $content_list_heading ): ?>

          <div class="row">
            <div class="col-12 col-lg-7">
              <h3 class="resources-subheading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.6s">
                <?php echo $content_list_heading; ?>
              </h3>
              <div class="resources-section-list resources-content-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                <?php echo $content_list; ?>
              </div>
            </div>
          </div>

        <?php else: ?>

          <div class="row">
            <div class="col-12 col-lg-7">
              <div class="resources-section-list resources-content-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                <?php echo $content_list; ?>
              </div>
            </div>
          </div>

        <?php endif; ?>

      <?php endif; ?>

      
      <?php if( $second_content_text || $second_content_subheading ): ?>

        <?php if( $second_content_subheading ): ?>

          <div class="row">
            <div class="col-12 col-lg-7">
              <h3 class="resources-subheading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.6s">
                <?php echo $second_content_subheading; ?>
              </h3>
              <div class="resources-section-text resources-content-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                <p><?php echo $second_content_text; ?></p>
              </div>
            </div>
          </div>

        <?php else: ?>

          <div class="row">
            <div class="col-12 col-lg-7">
              <div class="resources-section-text resources-content-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                <p><?php echo $second_content_text; ?></p>
              </div>
            </div>
          </div>

        <?php endif; ?>        

      <?php endif; ?>


      <?php if( $third_content_text || $third_content_subheading ): ?>

      <?php if( $third_content_subheading ): ?>

        <div class="row">
          <div class="col-12 col-lg-7">
            <h3 class="resources-subheading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.6s">
              <?php echo $third_content_subheading; ?>
            </h3>
            <div class="resources-section-text resources-content-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
              <?php echo $third_content_text; ?>
            </div>
          </div>
        </div>

      <?php else: ?>

        <div class="row extra-content">
          <div class="col-12 col-lg-7">
            <div class="resources-section-text extra-content resources-content-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
              <?php echo $third_content_text; ?>
            </div>
          </div>
        </div>

      <?php endif; ?>        

      <?php endif; ?>

      </div>
    </div>

  </div>

  <?php 
  get_footer();