<?php
/**
 * Template Name: Downloads
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

$args_cat = array(
    'post_type' => 'downloads',
    'posts_per_page' => 100,
    'taxonomy' => 'downloads_tax'
);
$cat = get_categories($args_cat);
$taxonomies = array(
    'downloads_tax',
);
$terms = get_terms($taxonomies);


?>
<div class="container downloads_page">
  <div class="row downloads_header os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
    <div class="col-12">
      <?php echo the_content() ?>
    </div>
  </div>
    <?php foreach ($terms as $key => $value): ?>
        <?php
        $args_post = array(
            'post_type'   => 'downloads',
            'posts_per_page' => 100,
            'tax_query' => array(
                array(
                    'taxonomy' => 'downloads_tax',
                    'field' => $value->name,
                    'terms' =>$value->term_id,
                )
            ),
        );
        $posts = get_posts( $args_post );

        ?>
      <div class="row no-gutters download_row os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
        <div class="col-8 offset-2 col-md-6 offset-md-573 col-lg-3 offset-lg-0">
          <div class="download_category">
            <img src="<?php echo z_taxonomy_image_url($value->term_id) ?>" />
            <h5><?php echo $value->name ?></h5>
          </div>
        </div>
        <div class="col-12 col-lg-5 download_center_parent ">
          <div class="nano nano_<?php echo $value->term_id ?>">
            <div class="download_center nano-content">
              <ul>
                  <?php $i = 0; foreach ($posts as $k2 => $v2):
                      $meta_value_1 = get_post_meta($v2->ID, 'redirect_url');
                      ?>

                    <li class="<?php if($i == 0) echo 'active' ?>" data-id="download_<?php echo $v2->ID ?>">
                        <?php echo $v2->post_title ?>
                    </li>
                      <?php $i++; endforeach; ?>
              </ul>
            </div>
          </div>

          <script>
            (function($) {
              $( document ).ready(function() {
                $(".nano_<?php echo $value->term_id ?>").nanoScroller();
              });

            })( jQuery );
          </script>
        </div>
        <div class="col-10 offset-1 col-md-8 offset-md-2 col-lg-4 offset-lg-0 download_right_parent">
          <div class="download_right_content">
              <?php $y = 0; foreach ($posts as $k2 => $v2):
                  $meta_value_1 = get_post_meta($v2->ID, 'redirect_url');
                  ?>
                <div id="download_<?php echo $v2->ID ?>" class="<?php if($y == 0) echo 'active' ?> text-center download_right_item">
                    <h5><?php if($v2->post_excerpt) echo $v2->post_excerpt; else echo $v2->post_title ?></h5>
                  <a href="<?php echo get_field('plik', $v2->ID)['url']?>" data-fancybox class="btn btn-warning">Download <span class="icon icon-caret-right"></span></a>
                </div>
              <?php $y++; endforeach; ?>
          </div>
        </div>
      </div>

    <?php endforeach; ?>
</div>





<?php
get_footer();


