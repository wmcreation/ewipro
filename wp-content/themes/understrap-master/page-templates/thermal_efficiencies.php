<?php
  /**
   * Template Name: Thermal Efficiencies
   *
   * Template for displaying a page just with the header and footer area and a "naked" content area in between.
   * Good for landingpages and other types of pages where you want to add a lot of custom markup.
   *
   * @package understrap
   */

  get_header();

  ?>

    <div class="thermal_efficiencies_wrapper">
      <div class="header-wrapper" style="background-image: url(<?php echo get_field('header_left_background', $post->ID); ?>)">
        <div class="container">

          <div class="row">
            <div class="col-12 col-lg-6">
              <h2 class="section-heading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">
                <span class="section-number"><span class="resources-number">01</span></span>
                <span class="white">
                  <?php echo get_field('header_left_heading', $post->ID); ?>
                </span>
              </h2>
            </div>
          </div>

          <div class="row">
            <div class="col-12 col-lg-5">
              <p class="resources-header-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                <?php echo get_field('header_left_text', $post->ID) ?>
              </p>
            </div>
          </div>       

        </div>
        <div class="d-none d-lg-block lazy right-side-background" data-src="<?php echo get_the_post_thumbnail_url($post->ID, 'full-hd'); ?>"></div>
      </div>

      <div class="content-wrapper">
        <div class="container">
          
          <div class="thickness_of_insulation_wrapper">

            <div class="row">
              <div class="col">
                <h3 class="resources-subheading thickness_of_insulation_heading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">
                  <?php echo get_field('thickness_of_insulation_heading', $post->ID); ?>
                </h3>
              </div>
            </div>

            <div class="row">
              <div class="col">
                <p class="resources-section-text resources-content-text thickness_of_insulation_text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                  <?php echo get_field('thickness_of_insulation_text', $post->ID); ?>
                </p>
              </div>
            </div>

            <div class="row">
              <div class="col-12 col-lg-7 col-xxl-6">
                <?php 
                  $table_thickness = get_field( 'thickness_of_insulation_table' );

                  if ( $table_thickness ) {
                    echo '<table class="table table-responsive thickness_of_insulation_table os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">';
                      if ( $table_thickness['header'] ) {

                        // Counting table heads
                        $headAmount = count($table_thickness['header']);
                        $middleHeading = $headAmount-2;
                        
                        echo '<thead>';
                          echo "<tr>
                            <th>U-value</th>
                            <th colspan='$middleHeading'>Thickness of insulation (mm) attached to 215mm solid brick wall</th>
                            <th>Requirement</th>
                          </tr>";
                          echo '<tr>';
                            foreach ( $table_thickness['header'] as $th ) {
                              echo '<th>';
                                echo $th['c'];
                              echo '</th>';
                            }
                          echo '</tr>';
                        echo '</thead>';
                      }
                      echo '<tbody>';
                        foreach ( $table_thickness['body'] as $tr ) {
                          echo '<tr>';
                            foreach ( $tr as $td ) {
                              echo '<td>';
                                echo $td['c'];
                              echo '</td>';
                            }
                          echo '</tr>';
                        }
                        echo "<tr>
                            <td colspan='$headAmount'>* Made up of 2 layers of insulation</td>
                          </tr>";
                      echo '</tbody>';
                    echo '</table>';
                  }
                ?>
              </div>
              <div class="col-12 col-lg-5 col-xxl-6">
                <p class="resources-section-text resources-content-text thickness_of_insulation_description os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                  <?php echo get_field('thickness_of_insulation_description', $post->ID); ?>
                </p>
              </div>
            </div>

          </div>

          <div class="thermal_conductivity_wrapper">

            <div class="row">
              <div class="col">
                <h3 class="resources-subheading thermal_conductivity_heading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">
                  <?php echo get_field('thermal_conductivity_heading', $post->ID); ?>
                </h3>
              </div>
            </div>

            <div class="row">
              <div class="col-12 order-12 order-lg-1 col-lg-6 col-xxl-5">
                <p class="resources-section-text resources-content-text thermal_conductivity_description os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                  <?php echo get_field('thermal_conductivity_description', $post->ID); ?>
                </p>
              </div>
              <div class="col-12 order-1 order-lg-12 col-lg-6 offset-xxl-1">
                <?php 
                  $table = get_field( 'thermal_conductivity_table' );
                  if ( $table ) {
                    echo '<table class="table table-responsive thermal_conductivity_table os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">';
                      if ( $table['header'] ) {
                        echo '<thead>';
                          echo '<tr>';
                            foreach ( $table['header'] as $th ) {
                              echo '<th>';
                                echo $th['c'];
                              echo '</th>';
                            }
                          echo '</tr>';
                        echo '</thead>';
                      }
                      echo '<tbody>';
                        foreach ( $table['body'] as $tr ) {
                          echo '<tr>';
                            foreach ( $tr as $td ) {
                              echo '<td>';
                                echo $td['c'];
                              echo '</td>';
                            }
                          echo '</tr>';
                        }
                      echo '</tbody>';
                    echo '</table>';
                  }
                ?>
              </div>
            </div>

          </div>

        </div>
      </div>
    </div>

  <?php 
  get_footer();