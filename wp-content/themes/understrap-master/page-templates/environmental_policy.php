<?php
  /**
   * Template Name: Environmental Policy
   *
   * Template for displaying a page just with the header and footer area and a "naked" content area in between.
   * Good for landingpages and other types of pages where you want to add a lot of custom markup.
   *
   * @package understrap
   */

  get_header();


  $args= array(
    'post_type' => 'page',
    'posts_per_page' => -1,
    'post_parent'    => $post->ID,
    'order'          => 'ASC',
    'orderby'        => 'menu_order'
  );
?>

<div class="environmental-policy-wrapper">
  <div class="header-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-6 extra-wrapper">
          <h2 class="section-heading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">
            <span class="section-number"><span class="resources-number">09</span></span>
            <span class="white">
              <?php echo get_field('section-header-heading', $post->ID) ?>
            </span>
          </h2>
          <div class="resources-header-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
            <?php echo get_field('section-header-text', $post->ID) ?>
          </div>
        </div>
      </div>
    </div>
    <div class="lazy right-side-background" data-src="<?php echo get_the_post_thumbnail_url($post->ID, 'full-hd') ?>)"></div>
  </div>

  <div class="content-wrapper">
    <div class="container">

      <div class="row">

        <div class="col-12 col-lg-7 extra-wrapper">
          <h3 class="resources-subheading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.6s">
            <?php echo get_field('section-content-subheading', $post->ID) ?>
          </h3>
          <div class="resources-section-list resources-content-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.6s">
            <?php echo get_field('section-content-list', $post->ID) ?>
          </div>
          <div class="resources-section-text resources-content-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.6s">
            <p><?php echo get_field('section-content-text', $post->ID) ?></p>
          </div>
        </div>

        <div class="col-10 offset-1 col-md-8 offset-md-2 col-lg-4 offset-lg-1">
          <div class="approval-box">
            <?php echo get_field('top_management_approval', $post->ID) ?>
          </div>

          <div class="d-flex align-items-center justify-content-between row text-center section-images">
            <div class="col">
              <div><img src="/wp-content/themes/understrap-master/assets/img/about-insulation-system-bba-logo-cert.png" alt="bba certificate logo"></div>
            </div>
            <div class="col">
              <div><img src="/wp-content/themes/understrap-master/assets/img/about-insulation-system-eta-logo.png" alt="eta logo"></div>
            </div>
          </div>

          <div class="d-flex align-items-center justify-content-between row text-center section-images">
            <div class="col">
              <span class="iso-text">ISO 9001:2015</span>
            </div>
            <div class="col">
              <div><img src="/wp-content/themes/understrap-master/assets/img/about-insulation-system-ce-logo.png" alt="eta logo"></div>
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>
</div>

<?php
get_footer();
