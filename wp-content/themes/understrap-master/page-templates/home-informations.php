<?php
/**
 * Template Name: Home informations
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

$args = array(
  'post_type'      => 'page',
  'posts_per_page' => -1,
  'post_parent'    => $post->ID,
  'order'          => 'ASC',
  'orderby'        => 'menu_order'
); ?>
<?php if(!get_field('section_on', $post->ID)): ?>
  <section id="<?php echo $post->post_name ?>">
    <div id="wrapper-informations">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h2 class="section-heading os-animation" data-os-animation="fadeInLeft" style="animation-delay: 0.3s;">
              <span class="section-number"><span class="informations-number">06</span></span>
              <span class="light-blue"><?php echo the_title() ?></span>
            </h2>
          </div>
        </div>
        <div class="row links-wrapper os-animation" data-os-animation="fadeInUp" style="animation-delay: 0.3s;">
          <div class="col-12 col-sm-6 col-lg-3 link-content">
            <a href="/downloads/">
              <img src="/wp-content/themes/understrap-master/assets/img/home-informations-data-sheets.svg" alt="data sheets logo" class="test">
              <h5><?php echo get_post_meta($post->ID, 'data-sheets-heading')[0]; ?></h5>
              <p><?php echo get_post_meta($post->ID, 'data-sheets-text')[0]; ?></p>
            </a>
          </div>
          <div class="col-12 col-sm-6 col-lg-3 link-content">
            <a href="/downloads/">
              <img src="/wp-content/themes/understrap-master/assets/img/home-informations-tech-drawings.svg" alt="data sheets logo" class="test">
              <h5><?php echo get_post_meta($post->ID, 'tech-drawings-heading')[0]; ?></h5>
              <p><?php echo get_post_meta($post->ID, 'tech-drawings-text')[0]; ?></p>
            </a>
          </div>
          <div class="col-12 col-sm-6 col-lg-3 link-content">
            <a href="/downloads/">
              <img src="/wp-content/themes/understrap-master/assets/img/home-informations-bba-certificates.svg" alt="data sheets logo" class="test">
              <h5><?php echo get_post_meta($post->ID, 'bba-certificates-heading')[0]; ?></h5>
              <p><?php echo get_post_meta($post->ID, 'bba-certificates-text')[0]; ?></p>
            </a>
          </div>
          <div class="col-12 col-sm-6 col-lg-3 link-content">
            <a href="/downloads/">
              <img src="/wp-content/themes/understrap-master/assets/img/home-informations-install-guides.svg" alt="data sheets logo" class="test">
              <h5><?php echo get_post_meta($post->ID, 'install-guides-heading')[0]; ?></h5>
              <p><?php echo get_post_meta($post->ID, 'install-guides-text')[0]; ?></p>
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>