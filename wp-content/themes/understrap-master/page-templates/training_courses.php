<?php
  /**
   * Template Name: Training Courses
   *
   * Template for displaying a page just with the header and footer area and a "naked" content area in between.
   * Good for landingpages and other types of pages where you want to add a lot of custom markup.
   *
   * @package understrap
   */

  get_header();


  $args = array(
    'post_type' => 'page',
    'posts_per_page' => -1,
    'post_parent'    => $post->ID,
    'order'          => 'ASC',
    'orderby'        => 'menu_order'
  );

  $args_post = array(
    'post_type'       => 'training_courses',
    'posts_per_page'  => 100
  );

  $posts = array_reverse(get_posts($args_post));
?>

<div class="training-courses-wrapper">
  <div class="header-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-md-6 extra-wrapper">
          <h2 class="section-heading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">
            <span class="section-number"><span class="resources-number">01</span></span>
            <span class="white">
              <?php echo get_field('section-header-heading', $post->ID) ?>
            </span>
          </h2>
          <div class="resources-header-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
            <?php echo get_field('section-header-text', $post->ID) ?>
          </div>
        </div>
      </div>
    </div>
    <div class="d-none d-md-block section-video lazy" data-src="<?php echo get_the_post_thumbnail_url($post->ID, 'full-hd') ?>)">
      <video loop="" id="video-training-header" muted="" plays-inline="" poster="<?php echo get_the_post_thumbnail_url($post->ID,'full-hd') ?>">
        <source src="<?php echo get_field('header-video', $post->ID) ?>" type="video/mp4">
      </video>
    </div>
  </div>

  <div class="why-ewipro-wrapper d-flex align-items-center">
    <div class="container">
      <div class="row">
        <div class="col-lg-4">
          <h3 class="section-heading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.6s">
            <span class="light-blue"><?php echo get_field('section-content-subheading', $post->ID) ?></span>
          </h3>
        </div>
        <div class="col-lg-7 offset-lg-1">
          <div class="resources-content-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.6s">
            <?php echo get_field('section-content-text', $post->ID) ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php foreach($posts as $key => $value): ?>
    <div class="course-wrapper">
      <div class="container">
        <div class="row">
          <div class="col-md-6 left-side">
            <div class="course-label small-heading os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.6s">
              <span class="white"><?php echo get_field('course-label', $value->ID) ?></span>
            </div>
            <h4 class="course-title section-heading os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.6s">
              <span class="white"><?php echo get_field('course-title', $value->ID) ?></span>
            </h4>
          </div>
          <div class="col-md-5 offset-md-1 right-side<?php if($key == (count( $posts ) -1)) echo ' last' ?>">
            <p class="course-right-text-1 os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.9s">
              <?php echo get_field('course-right-text-1', $value->ID) ?>
            </p>
            <h5 class="course-right-subheading os-animation" data-os-animation="fadeInRight" data-os-animation-delay="0.9s">
              <?php echo get_field('course-right-subheading', $value->ID) ?>
            </h5>
            <div class="course-right-list os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.9s">
              <?php echo get_field('course-right-list', $value->ID) ?>
            </div>
            <?php 
              $content = get_field('course-right-text-2', $value->ID);
              if(!empty($content)): ?>
                <p class="course-right-text-2 os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.9s"><?php echo $content ?></p>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <div class="left-side-bg os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.6s" style="background-image: url(<?php echo get_field('course-image', $value->ID); ?>)"></div>
    </div>
  <?php endforeach; ?>
  </div>
</div>

<?php 
get_footer();