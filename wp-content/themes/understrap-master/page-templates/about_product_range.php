<?php
/**
 * Template Name: About Product Range
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */
$args = array(
  'post_type'      => 'page',
  'posts_per_page' => -1,
  'post_parent'    => $post->ID,
  'order'          => 'ASC',
  'orderby'        => 'menu_order'
); 

?>

<div id="wrapper-product-range">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-5 offset-lg-7">
        <h2 class="small-heading right-heading os-animation" data-os-animation="fadeInRight" data-os-animation-delay="0.3s">
          <?php echo get_post_meta($post->ID, 'product-range-heading')[0]; ?>
        </h2>
      </div>
    </div>
    <div class="row">
    <div class="col-12 order-12 order-lg-0 col-lg-5">
        <h3 class="small-heading left-heading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">
          <span class="white"><?php echo get_post_meta($post->ID, 'product-range-leftside-heading')[0]; ?></span>
        </h3>
      </div>
      <div class="col-12 col-lg-5 offset-lg-2">
        <div class="row">
          <div class="col">
            <div class="content-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
              <?php echo the_content(); ?>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col"> 
            <h5 class="include-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.6s">
              <?php echo get_post_meta($post->ID, 'product-range-include-text')[0]; ?>
            </h5>
          </div>
        </div>
        <div class="include-options row os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.8s">
          <div class="col-6 col-lg-4 option-box">
            <p class="option"><?php echo get_post_meta($post->ID, 'product-range-include-option-1')[0]; ?></p>
          </div>
          <div class="col-6 col-lg-4 option-box">
            <p class="option"><?php echo get_post_meta($post->ID, 'product-range-include-option-2')[0]; ?></p>
          </div>
          <div class="col-6 col-lg-4 option-box">
            <p class="option"><?php echo get_post_meta($post->ID, 'product-range-include-option-3')[0]; ?></p>
          </div>
          <div class="col-6 col-lg-4 option-box">
            <p class="option"><?php echo get_post_meta($post->ID, 'product-range-include-option-4')[0]; ?></p>
          </div>
          <div class="col-6 col-lg-4 option-box">
            <p class="option"><?php echo get_post_meta($post->ID, 'product-range-include-option-5')[0]; ?></p>
          </div>
          <div class="col-6 col-lg-4 option-box text-center">
            <a href="#" class="option link d-flex align-items-center align-items-xl-end">View our products <i class="icon-caret-right"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="section-left-side lazy" data-src="<?php echo get_the_post_thumbnail_url(); ?>"></div>
  <div class="section-right-side"></div>
</div>

 