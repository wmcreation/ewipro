<?php
/**
 * Template Name: Home Company News
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

$posts = array(
  'numberposts' => 3,
  'offset' => 0,
  'category' => 0,
  'orderby' => 'post_date',
  'order' => 'desc',
  'include' => '',
	'exclude' => '',
	'meta_key' => '',
	'meta_value' =>'',
	'post_type' => 'post',
	'post_status' => 'draft, publish, future, pending, private',
	'suppress_filters' => true
); 

$recents_posts = wp_get_recent_posts($posts);

?>
<?php if(!get_field('section_on', $post->ID)): ?>
  <section id="<?php echo $post->post_name; ?>" style="background-image: url(<?php echo get_the_post_thumbnail_url($post->ID, 'full-hd'); ?>)">
    <div class="container">

      <div class="row">
        <div class="col text-center">
          <h2 class="section-heading os-animation"  data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">
            <span class="section-number">
              <span class="company-news-number">07</span>
            </span>
            <span class="white">
              <?php echo get_the_title($post->ID); ?>
            </span>
          </h2>
        </div>
      </div>

      <div class="row os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.4s">
        <?php foreach($recents_posts as $post): ?>

          <article <?php post_class('col-md-6 col-lg-4 post_item'); ?> id="post-<?php echo $post['ID']; ?>">
            <a href="<?php echo the_permalink($post['ID'])?>">
              <div class="post_item_date">
                <span class="icon icon-calendar"></span>
                <span>
                  <?php echo get_the_date('d.m.Y', $post['ID']) ?>
                </span>
              </div>

              <div class="position-relative">
                <header class="entry-header">


                  <!--		--><?php //the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
                    //		'</a></h2>' ); ?>


                </header><!-- .entry-header -->

                  <?php echo get_the_post_thumbnail( $post['ID'], 'blog_thumb' ); ?>

                <div class="entry-content">

                    <p><?php echo get_the_title($post['ID']); ?></p>

                    <?php
                    wp_link_pages( array(
                        'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
                        'after'  => '</div>',
                    ) );
                    ?>

                </div><!-- .entry-content -->


              </div>

              <footer class="entry-footer">

                <span class="blog_item_link">READ ARTICLE</span>

              </footer><!-- .entry-footer -->
            </a>
          </article><!-- #post-## -->

        <?php endforeach; ?>
      </div>

      <div class="row">
        <div class="col text-center">
          <div class="button">
            <a href="/blog/" class="read-more-button">view all posts</a>
          </div>
        </div>
      </div>

    </div>
  </section>
<?php endif; ?>