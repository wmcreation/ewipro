<?php
/**
 * Template Name: Footer
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

 ?>

 <div id="wrapper-footer">
  <div class="container">
    <div class="row">
      <div class="col-3">
        <div class="logo">
          <img src="<?php echo get_the_post_thumbnail_url($post->ID,'full-hd') ?>" alt="logo">
        </div>
      </div>
      <div class="col">
        test
      </div>
      <div class="col">
        test
      </div>
      <div class="col">
        test
      </div>
      <div class="col">
        test
      </div>
    </div>
  </div>
</div>
