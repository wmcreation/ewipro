<?php
/**
 * Template Name: Homepage
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

?>

<?php

$args = array (
    'post_type'                => 'page',
    'post_parent'            => $post->ID,
    'order'                  => 'ASC',
    'orderby'        => 'menu_order'
);

$query = new WP_Query( $args );
?>
    <aside  class="home_aside scrollspy_menu">
        <ul class="navbar-nav">
            <?php
            $i = 1;
            if ( $query->have_posts() ) {
                while ( $query->have_posts() ) {

                    $query->the_post();

                    ?>
                    <?php if(!get_field('section_on', $post->ID)): ?>
                      <li class="nav-item">
                          <a href="#<?php echo $post->post_name ?>" class="nav-link">
                              <?php echo '0'.$i ?>
                          </a>
                      </li>
                    <?php endif; ?>
                    <?php
                    $i++;
                }
            };
            ?>
        </ul>
    </aside>

<?php
if ( $query->have_posts() ) {
    while ( $query->have_posts() ) {

        $query->the_post();
        $tn = get_page_template_slug( $post->ID );
        $word = array("page-", ".php",' ');
        $template = str_replace($word,'',$tn);

        get_template_part('page', $template);
    }
};
?>


<?php
get_footer();


