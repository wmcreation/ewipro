<?php
/**
 * Template Name: About Insulation System
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */
$args = array(
  'post_type'      => 'page',
  'posts_per_page' => -1,
  'post_parent'    => $post->ID,
  'order'          => 'ASC',
  'orderby'        => 'menu_order'
); 

?>

<div id="wrapper-insulation-system">
  <div class="background lazy" data-src="<?php echo get_the_post_thumbnail_url(); ?>"></div>
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-7">
        <h1 class="section-heading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">
          <?php echo get_post_meta($post->ID, 'about-insulation-system-heading')[0]; ?>
        </h1>
      </div>
    </div>
    <div class="row no-gutters">
      <div class="col-12 col-lg-5">
        <h5 class="section-subheading os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
          <?php echo get_post_meta($post->ID, 'about-insulation-system-subheading')[0]; ?>
        </h5>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-lg-6">
        <div class="content-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
          <?php echo the_content(); ?>
        </div>
      </div>
    </div>
    <div class="row no-gutters">
      <div class="col-12 col-lg-6 section-images-wrapper">
        <div class="section-images os-animation d-flex align-items-center justify-content-center" data-os-animation="fadeInLeft" data-os-animation-delay="0.6s">
          <div class="section-image"><img src="/wp-content/themes/understrap-master/assets/img/about-insulation-system-bba-logo-cert.png" alt="bba certificate logo"></div>
          <div class="section-image"><img src="/wp-content/themes/understrap-master/assets/img/about-insulation-system-eta-logo.png" alt="eta logo"></div>
          <div class="section-image"><img src="/wp-content/themes/understrap-master/assets/img/about-insulation-system-ce-logo.png" alt="ce logo"></div>
        </div>
      </div>
    </div>
  </div>
</div>
