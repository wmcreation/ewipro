<?php
  /**
   * Template Name: Render Textures
   *
   * Template for displaying a page just with the header and footer area and a "naked" content area in between.
   * Good for landingpages and other types of pages where you want to add a lot of custom markup.
   *
   * @package understrap
   */

  get_header();


  $args = array (
    'post_type' => 'page',
    'posts_per_page' => -1,
    'post_parent'    => $post->ID,
    'order'          => 'ASC',
    'orderby'        => 'menu_order'
  );

  $args_cat = array (
    'post_type' => 'render_textures_type',
    'posts_per_page' => 100,
    'taxonomy' => 'render_tax'
  );
  $cat = get_categories($args_cat);
  $taxonomies = array (
    'taxonomy' => 'render_tax',
    'hide_empty' => false
  );
  $terms = array_reverse(get_terms($taxonomies));
?>

<div class="render-textures-wrapper">
  <div class="header-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-6">
          <h2 class="section-heading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">
            <span class="light-blue"><?php echo get_field('header-heading', $post->ID) ?></span>
          </h2>
          <div class="header-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
            <?php echo get_field('header-text', $post->ID) ?>
          </div>
        </div>
      </div>
    </div>
    <div class="d-none d-lg-block lazy right-side" data-src="<?php echo get_the_post_thumbnail_url($post->ID) ?>)"></div>
  </div>

  <div class="render-types-wrapper">
    <div class="container">
      <div class="row">
				<?php foreach($terms as $key => $value): ?>
          <div class="col-12 col-sm-6 col-md-4 offset-md-0">
            <h3 class="render-type-heading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.6s"><?php echo $value->name; ?></h3>
            <figure class="image os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.6s">
							<a href="<?php echo z_taxonomy_image_url($value->term_id, 'large') ?>" data-fancybox="gallery" data-caption="<?php echo $value->name ?>">
								<img src="<?php echo z_taxonomy_image_url($value->term_id, 'render-tax-thumb') ?>" alt="<?php echo $value->name . ' image' ?>">
							</a>
              <?php if(!empty( $value->description )): ?>
                <figcaption><?php echo $value->description ?></figcaption>
              <?php endif; ?>
            </figure>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>

  <div class="render-colour-chart-wrapper">
		<div class="renders-header">
			<div class="container">
				<div class="row">
					<div class="col-12 order-12 order-md-1 col-md-7 col-lg-6">
						<h2 class="section-heading">
							<span class="light-blue os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.6s"><?php echo get_field('colour-chart-heading', $post->ID) ?></span>
						</h2>
						<div class="colour-chart-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.6s">
							<?php echo get_field('colour-chart-text', $post->ID) ?>
						</div>
					</div>
					<div class="col-12 offset-0 order-1 order-md-12 col-md-4 offset-md-1 offset-lg-2">
						<h3 class="colour-chart-summary os-animation" data-os-animation="fadeInRight" data-os-animation-delay="0.6s">
							<?php echo get_field('colour-chart-summary', $post->ID) ?>
						</h3>
					</div>
				</div>
			</div>
		</div>

    <div class="renders-list">
      <div class="container wrap">
        <?php foreach($terms as $key => $value): ?>
          <?php
            $args_post = array (
              'post_type'   => 'render_textures_type',
              'posts_per_page' => 100,
              'tax_query' => array (
                array (
                    'taxonomy' => 'render_tax',
                    'field' => $value->name,
                    'terms' => $value->term_id,
                )
              ),
            );
            $posts = array_reverse(get_posts( $args_post ));
          ?>
          <?php if($value->count > 0): ?>
            <h2 class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s"><?php echo $value->name ?></h2>
            <div class="row">
              <?php foreach($posts as $key2 => $value2): ?>
                <?php
                  $attachment_id = get_field('image', $value2->ID);
                  $thumb_size = 'render-thumb';
                  $size = 'large';
                  $thumb_image = wp_get_attachment_image_src($attachment_id, $thumb_size);
                  $image = wp_get_attachment_image_src($attachment_id, $size);
                ?>
                <div class="col">
                  <figure class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                    <a href="<?php echo $image[0] ?>" data-fancybox="renders-gallery" 
                      data-caption="<?php echo 'Type: ' . $value2->post_title . ' Product number: ' . get_field('catalog-number', $value2->ID) ?>">
                      <img src="<?php echo $thumb_image[0] ?>" alt="<?php echo $value2->post_title . ' image' ?>">
                    </a>
                    <figcaption><?php echo $value2->post_title ?></figcaption>
                    <span class="catalog-number"><?php echo get_field('catalog-number', $value2->ID) ?></span>
                  </figure>
                </div>
              <?php endforeach; ?>
            </div>
          <?php endif; ?>
        <?php endforeach; ?>
      </div>
    </div>

    <div class="differences-caution">
      <div class="container">
        <div class="row os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
				  <div class="col-2 col-md-5 text-right">
						<img class="differences-caution-image" src="<?php echo get_field('differences-caution-image', $post->ID) ?>" alt="differences caution image">
					</div>
					<div class="col-10 col-md-7 d-flex align-items-center">
						<span class="differences-caution-text"><?php echo get_field('differences-caution-text', $post->ID) ?></span>
					</div>
        </div>
      </div>
    </div>
	</div>

	<div class="render-order-wrapper os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s" style="background-image: url(<?php echo get_field('order-background', $post->ID) ?>)">
		<div class="animation-wrap">
			<div class="white-box text-center">
				<h4 class="order-heading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s"><?php echo get_field('order-heading', $post->ID) ?></h4>
				<p class="order-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s"><?php echo get_field('order-text', $post->ID) ?></p>
				<a href="https://ewistore.co.uk/shop/coloured-render-colour-chart/" class="read-more-button os-animation" target="_blank" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">order now</a>
			</div>
		</div>
	</div>
</div>

<?php

get_footer();
