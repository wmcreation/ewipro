<?php
/**
 * Template Name: Find an Installer
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */
get_header();
$args= array(
    'post_type' => 'page',
    'posts_per_page' => -1,
    'post_parent'    => $post->ID,
    'order'          => 'ASC',
    'orderby'        => 'menu_order'
);
?>

<div class="find-installer-wrapper">
    <div class="header-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 extra-wrapper">
                    <h2 class="section-heading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">
                        <span class="section-number"><span class="resources-number">09</span></span>
                        <span class="white">
                            <?php echo get_field('section-header-heading', $post->ID) ?>
                        </span>
                    </h2>
                    <div class="resources-header-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                        <?php echo get_field('section-header-text', $post->ID) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-none d-lg-block lazy right-side-background" data-src="<?php echo get_the_post_thumbnail_url($post->ID, 'full-hd') ?>"></div>
    </div>

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12 heading-wrapper">
                    <h2 class="content-heading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">
                        <?php echo get_field('section-content-header', $post->ID) ?>
                    </h2>
                </div>
            </div>
            <div class="row no-gutters form-wrapper">
                <div class="col-12 col-lg-7 form-info-wrapper">
                    <div class="single-box os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                        <span class="box-number">1</span>
                        <div class="box-image">
                            <img src="<?php echo get_field('section-content-form-info-icon-1', $post->ID) ?>" alt="ewipro installer request form icon">
                        </div>
                        <span class="box-text"><?php echo get_field('section-content-form-info-text-1', $post->ID) ?></span>
                    </div>
                    <div class="single-box os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                        <span class="box-number">2</span>
                        <div class="box-image">
                            <img src="<?php echo get_field('section-content-form-info-icon-2', $post->ID) ?>" alt="ewipro installer request form icon">
                        </div>
                        <span class="box-text"><?php echo get_field('section-content-form-info-text-2', $post->ID) ?></span>
                    </div>
                    <div class="single-box os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                        <span class="box-number">3</span>
                        <div class="box-image">
                            <img src="<?php echo get_field('section-content-form-info-icon-3', $post->ID) ?>" alt="ewipro installer request form icon">
                        </div>
                        <span class="box-text"><?php echo get_field('section-content-form-info-text-3', $post->ID) ?></span>
                    </div>
                    <div class="single-box os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                        <span class="box-number">4</span>
                        <div class="box-image">
                            <img src="<?php echo get_field('section-content-form-info-icon-4', $post->ID) ?>" alt="ewipro installer request form icon">
                        </div>
                        <span class="box-text"><?php echo get_field('section-content-form-info-text-4', $post->ID) ?></span>
                    </div>
                    <div class="single-box os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                        <span class="box-number">5</span>
                        <div class="box-image">
                            <img src="<?php echo get_field('section-content-form-info-icon-5', $post->ID) ?>" alt="ewipro installer request form icon">
                        </div>
                        <span class="box-text"><?php echo get_field('section-content-form-info-text-5', $post->ID) ?></span>
                    </div>
                </div>
                <div class="col-12 col-lg-5 form-content-wrapper os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                    <h4 class="form-heading">Installers Request Form</h4>
                    <form>
                        <div class="form-row">
                            <input type="text" class="form-input" placeholder="Your Name">
                        </div>
                        <div class="form-row">
                            <input type="email" class="form-input" placeholder="E-mail address">
                        </div>
                        <div class="form-row">
                            <input type="tel" class="form-input" placeholder="Phone number">
                        </div>
                        <div class="form-row">
                            <input type="number" class="form-input" placeholder="Postcode">
                        </div>
                        <div class="form-row">
                            <input type="checkbox" id="change" name="change" class="form-checkbox">
                            <label for="change">Want to change how you receive these emails?</label>
                        </div>
                        <div class="form-submit">
                            <button type="submit" class="form-submit__button">
                                Send
                                <i class="icon icon-caret-right"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row text-wrapper">
                <div class="col-12 col-lg-7 extra-wrapper">
                    <h3 class="resources-subheading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.6s">
                      <?php echo get_field('section-content-subheading', $post->ID) ?>
                    </h3>
                    <div class="resources-section-text resources-content-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.6s">
                      <?php echo get_field('section-content-text', $post->ID) ?>
                    </div>
                </div>
                <div class="col-12 col-lg-5 pushed-to-bottom">
                    <h3 class="resources-subheading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.6s">
                      <?php echo get_field('section-content-subheading2', $post->ID) ?>
                    </h3>
                    <div class="resources-section-list resources-content-text os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.6s">
                      <?php echo get_field('section-content-list', $post->ID) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

get_footer();
