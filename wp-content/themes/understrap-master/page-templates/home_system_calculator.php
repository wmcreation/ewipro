<?php
/**
 * Template Name: Home system calculator
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */
$args = array(
  'post_type'      => 'page',
  'posts_per_page' => -1,
  'post_parent'    => $post->ID,
  'order'          => 'ASC',
  'orderby'        => 'menu_order'
); ?>
<?php if(!get_field('section_on', $post->ID)): ?>
  <section id="<?php echo $post->post_name ?>">
    <div id="wrapper-system-calculator">
      <div class="container position-relative">
        <div class="row">
          <div class="col">
            <div class="system-calculator-text-box">
              <h2 class="section-heading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">
                <span class="section-number"><span class="system-number">03</span></span>
                <span class="white"><?php echo the_title() ?></span>
              </h2>
              <div class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
                  <?php echo the_content() ?>
              </div>
              <a href="/system-calculator/" class="read-more-button os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">go to calculator</a>
            </div>
          </div>
        </div>
        <img src="/wp-content/themes/understrap-master/assets/img/home-system-calculator-house-tablet.png" class="right-img os-animation" data-os-animation="fadeInRight" data-os-animation-delay="0.3s">
      </div>
    </div>
  </section>
<?php endif; ?>