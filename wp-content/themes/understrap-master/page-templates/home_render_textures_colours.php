<?php
/**
 * Template Name: Home render textures & colours
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */
$args = array(
  'post_type'      => 'page',
  'posts_per_page' => -1,
  'post_parent'    => $post->ID,
  'order'          => 'ASC',
  'orderby'        => 'menu_order'
); ?>
<?php if(!get_field('section_on', $post->ID)): ?>
  <section id="<?php echo $post->post_name ?>">
    <div id="wrapper-render-textures-colours" style="background-image: url(<?php echo get_template_directory_uri() . '/assets/img/home-render-textures-colours-left-min.png' ?>)" class="os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">
      <div class="container">
        <div class="row no-gutters">
          <div class="col-12 col-sm-7 offset-sm-5">
            <div class="render-textures-content">
              <h2 class="section-heading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">
                <span class="section-number"><span class="render-textures-number">04</span></span>
                <span class="light-blue">
                  <?php echo the_title() ?>
                </span>
              </h2>
              <div class="render-text">
                <div class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s"><?php echo the_content() ?></div>
              </div>
              <a href="/render-textures/" class="read-more-button os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">Choose your color</a>
            </div>
          </div>
        </div>
      </div>
      <img src="<?php echo get_template_directory_uri() . '/assets/img/home-render-textures-colours-right-cropped.png' ?>" class="right-image os-animation" data-os-animation="fadeInRight" data-os-animation-delay="0.3s">
    </div>
  </section>
<?php endif; ?>