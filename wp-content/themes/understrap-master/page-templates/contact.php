<?php
  /**
   * Template Name: Contact
   *
   * Template for displaying a page just with the header and footer area and a "naked" content area in between.
   * Good for landingpages and other types of pages where you want to add a lot of custom markup.
   *
   * @package understrap
   */

  get_header();

  $args = array(
    'post_type' => 'page',
    'posts_per_page' => -1,
    'post_parent'    => $post->ID,
    'order'          => 'ASC',
    'orderby'        => 'menu_order'
  );

?>

<div class="contact-wrapper">
  <div id="map"></div>

  <div id="iconLink" data-link="<?php echo get_field('map-icon', $post->ID); ?>"></div>
  <div class="background"></div>

  <div class="informations-wrapper os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">
    <div class="container position-relative">
      <div class="row contact-info-box no-gutters">

        <div class="col-10 offset-1 col-sm-6 offset-sm-3 offset-md-0 col-md-5 col-lg-4 col-xl-3 address">
          <div class="company-name">
            <?php echo get_field('company-name', $post->ID) ?>
          </div>
          <div class="company-address">
            <span><?php echo get_field('company-address', $post->ID) ?></span>
          </div>
        </div>

        <div class="col-6 offset-md-0 col-md-3 col-lg-4 col-xl-2">
          <div class="technical-support-heading">
            <?php echo get_field('technical-support-heading', $post->ID) ?>
          </div>
          <div class="technical-support-contact">
            <div class="wrap">
              <img src="<?php echo get_field('mobile-icon', $post->ID) ?>" alt="mobile icon">
              <a href="tel:<?php echo get_field('technical-support-mobile', $post->ID) ?>">
                <span>
                  <?php echo get_field('technical-support-mobile', $post->ID) ?>
                </span>
              </a>
            </div>
            <div class="wrap">
              <img src="<?php echo get_field('email-icon', $post->ID) ?>" alt="email icon">
              <a href="mailto:<?php echo get_field('technical-support-email', $post->ID) ?>">
                <span>
                  <?php echo get_field('technical-support-email', $post->ID) ?>
                </span>
              </a>
            </div>
          </div>
        </div>

        <div class="col-6 col-md-3 offset-md-1 col-lg-4 offset-lg-0 col-xl-2">
          <div class="sales-inquires-heading">
            <?php echo get_field('sales-inquires-heading', $post->ID) ?>
          </div>
          <div class="sales-inquires-contact">
            <div class="wrap">
              <img src="<?php echo get_field('mobile-icon', $post->ID) ?>" alt="mobile icon">
              <a href="tel:<?php echo get_field('sales-inquires-mobile', $post->ID) ?>">
                <span>
                  <?php echo get_field('sales-inquires-mobile', $post->ID) ?>
                </span>
              </a>
            </div>
            <div class="wrap">
              <img src="<?php echo get_field('email-icon', $post->ID) ?>" alt="email icon">
              <a href="mailto:<?php echo get_field('sales-inquires-email', $post->ID) ?>">
                <span>
                  <?php echo get_field('sales-inquires-email', $post->ID) ?>
                </span>
              </a>
            </div>
          </div>
        </div>

      </div>
      <div class="d-none d-xl-block contact-form-wrapper">
        <h2 class="contact-heading">
          Contact Form
        </h2>
        <?php echo do_shortcode(get_field('contact-form', $post->ID)); ?>
      </div>
    </div>

  </div>
</div>

<div class="d-block d-xl-none contact-form-wrapper os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
  <h2 class="contact-heading">
    Contact Form
  </h2>
  <?php echo do_shortcode(get_field('contact-form', $post->ID)); ?>
</div>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD23L5-NUZDMw6JMbuWIkfscLHhoaqxL0k&callback=initMap"></script>
<script>

  jQuery(document).ready(function($) {
    // Contact - Google Maps Init
    function initMap() {
      var ewiStore = { lat: 51.3728, lng: -0.2989 };
      var mapCenter = {lat: 51.3746, lng: -0.2989 };
      var map = new google.maps.Map (
          document.getElementById('map'), { zoom: 16, center: mapCenter }
      );
      var icon = document.getElementById('iconLink').dataset.link;
      var marker = new google.maps.Marker({ position: ewiStore, icon: icon, map: map });
    }

    initMap();
  });





</script>
<?php
get_footer();
