<?php
/**
 * Template Name: Home slider
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */
$args = array(
    'post_type'      => 'page',
    'posts_per_page' => -1,
    'post_parent'    => $post->ID,
    'order'          => 'ASC',
    'orderby'        => 'menu_order'
);


$parent = new WP_Query( $args );

if ( $parent->have_posts() ) : ?>

<section id="<?php echo $post->post_name ?>">
  <div class="home_slider">
    <div class="swiper-container home_start_slider os-animation" data-os-animation="fadeIn" data-os-animation-delay="0.3s">
      <div class="swiper-wrapper">
          <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
            <div class="swiper-slide" style="background-image: url(<?php echo get_the_post_thumbnail_url($post->ID,'full-hd') ?>)">
              <span class="white-center">
                <div class="text-box">
                  <div class="text">
                    <p><?php echo get_post_meta($post->ID, 'slide_title')[0]; ?></p>
                    <?php if( get_field('slide_link', $post->ID) ): ?>
                      <a href="<?php echo get_field('slide_link', $post->ID); ?>" class="learn-more">Learn more</a>
                    <?php endif; ?>
                  </div>
                </div>
              </span>
            </div>
          <?php endwhile; ?>
      </div>
      <!--<div class="container swiper-pagination-container">-->
        <div class="swiper-pagination"></div>
      <!-- </div> -->
      </div>
    </div>
    <a href="#wrapper-about" class="next_section">
      <span class="icon icon-arrow-down"></span>
    </a>
</section>
<?php endif; wp_reset_postdata(); ?>



