<?php
/**
 * Template Name: Products
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

$args = array(
  'post_type' => 'page',
  'posts_per_page' => -1,
  'post_parent' => $post->ID
);

$args_terms = array(
  'taxonomy' => 'products_tax',
  'hide_empty' => false
);

$args_posts = array(
  'post_type' => 'ewi_product',
  'posts_per_page' => -1
);

$terms = get_terms($args_terms);
$posts = get_posts($args_posts);
?>

<div class="products_list_wrapper">
  <?php foreach($terms as $key => $term): ?>
    <div class="product_category_wrapper <?php if( $key%2 == 0 ) echo 'even'; else echo 'odd'; ?>" 
      style="background-image: url(<?php echo z_taxonomy_image_url($term->term_id); ?>)">
      <div class="container">
        <div class="row product_category">
          <h2 class="section-heading">
            <span class="section-number"><span class="about-number"><?php echo '0' . ++$key ?></span></span>
            <?php echo $term->name; ?>
          </h2>
        </div>
        <div class="row product_category_images ">
          <?php foreach($posts as $post): ?>
            <?php if( wp_get_post_terms($post->ID, 'products_tax')[0]->name == $term->name ): ?>
              <div class="col-6 col-md-4 col-lg-3 single_image text-center ">
                <a href="<?php echo get_permalink($post->ID); ?>" class="product_link">
                  <img src="<?php echo get_the_post_thumbnail_url($post->ID, 'medium'); ?>" alt="<?php echo get_the_title($post->ID) . ' image'; ?>">
                  <h3 class="image_title"><?php echo $post->post_title; ?></h3>
                </a>
              </div>
            <?php endif; ?>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  <?php endforeach; ?> 
</div>

<?php
get_footer();