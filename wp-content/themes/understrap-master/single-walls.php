<?php
  /**
   * Template Name: Wall's Build Up
   *
   * Template for displaying a page just with the header and footer area and a "naked" content area in between.
   * Good for landingpages and other types of pages where you want to add a lot of custom markup.
   *
   * @package understrap
   */

   $current_post_id = $post->ID;
   $current_term_name = wp_get_post_terms($post->ID, 'walls_tax')[0]->name;

  $args_cat = array(
    'taxonomy' => 'walls_tax',
    'hide_empty' => false
  );

  $terms = get_terms($args_cat);

  get_header();

  $pointsList = array(
    array()
  );
  for($i = 1; $i <= 12; $i++) {
    $posXName = 'layer_' . $i . '_posX';
    $posYName = 'layer_' . $i . '_posY';
    $posX = get_field($posXName, $current_post_id);
    $posY = get_field($posYName, $current_post_id);
    if( $posX && $posY ) {
      $pointsList[$i-1]['posX'] = $posX;
      $pointsList[$i-1]['posY'] = $posY;
    }
  }

  ?>
  <div id="wrapper_walls_build_up">

    <div class="section-header">
      <div class="container">
        <div class="row align-items-center">

          <div class="col-4">
            <h2 class="section-heading os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">
              <?php echo get_the_title(635); ?>
            </h2>
          </div>

          <div class="col-7 offset-1 section-header-text">
            <?php echo get_post_field('post_content', 635); ?>
          </div>

        </div>
      </div>
    </div>

    <div class="section-content">
      <div class="container">
        <div class="row">

          <div class="col-3">
            <div class="systems-list">
							<h3 class="os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.45s">choose system</h3>
							<ul class="main-list">
								<?php foreach($terms as $key => $term): ?>

                  <?php
                    wp_reset_query();
                    $args_post = array(
                      'post_type'   => 'walls',
                      'posts_per_page' => -1,
                      'tax_query' => array(
                          array(
                              'taxonomy' => 'walls_tax',
                              'field' => $term->term_id,
                              'terms' =>$term->name,
                          )
                      ),
                  );
                  $posts = new WP_Query($args_post);
                ?>

                  <?php if( $posts->have_posts() ): ?>

                    <li class="<?php if($current_term_name == $term->name) echo 'is-active'; ?>"  
                    data-toggle="collapse" data-target="#system<?php echo $key; ?>" aria-expanded="false" aria-controls="system<?php echo $key; ?>">
                      <?php echo $term->name ?>

                      <ul class="collapse <?php if( $current_term_name == $term->name ) echo 'show'; ?>" id="system<?php echo $key; ?>">
                      <?php while( $posts->have_posts() ): $posts->the_post(); ?>
                        <?php if( get_the_terms($post->ID, 'walls_tax')[0]->name == $term->name ): ?>
                          <li>
                            <a class="<?php if($current_post_id == $post->ID) echo 'active' ?>" href="<?php echo get_permalink($post->ID); ?>"><?php echo get_field('walls_name', $post->ID); ?></a>
                          </li>
                        <?php endif; ?>
                      <?php endwhile; ?>
                      </ul>
                    </li>

                  <?php endif; ?>

                <?php endforeach; ?>
              </ul>
            </div>
          </div>

    			<div class="col-8 offset-1">
            <div class="row no-gutters">
              <div class="col-9">
                <div class="image-wrapper">
                  
                  <img src="<?php echo get_field('wall_section_image', $current_post_id) ?>" alt="<?php echo $current_term_name . ' ' . get_field('walls_name', $current_post_id) . ' image' ?>" class="image">

                  <?php $imageSize = getimagesize(get_field('wall_section_image', $current_post_id)); ?>
                  <?php $imageWidth = $imageSize[0] ?>
                  <?php $imageHeight = $imageSize[1] ?>
                  <?php foreach($pointsList as $key => $value): ?>

                    <div class="image-point" data-current="<?php echo $key+1; ?>" 
                    style="top: <?php echo $value['posY'] ?>%; left: <?php echo $value['posX'] ?>%">

                      <span class="icon-box"><i class="icon icon-plus"></i></span>
                      <div class="image-point-content 
                        <?php if( $value['posX'] < 50 ) echo 'right'; else echo 'left'; ?> 
                        <?php if( $value['posY'] < 50 ) echo 'bottom'; else echo 'top'; ?>
                      ">
                      
                        <?php
                          $current_layer_number = $key+1;
                          $current_layer_tooltip = 'layer_' . $current_layer_number . '_tooltip';
                          $current_layer_name = 'layer_' . $current_layer_number . '_name';
                          $current_layer_tooltip_link = 'layer_' .$current_layer_number . '_tooltip_link';
                        ?>
                        <h3 class="content-heading"><?php echo get_field($current_layer_name, $current_post_id); ?></h3>
                        <p class="content-text"><?php echo get_field($current_layer_tooltip, $current_post_id); ?></p>
                        <?php if( get_field($current_layer_tooltip_link, $current_post_id) ): ?>
                          <a href="<?php echo get_field($current_layer_tooltip_link, $current_post_id); ?>" class="learn-more" target="_blank">
                            Learn more 
                            <i class="icon icon-caret-right"></i>
                          </a>
                        <?php endif; ?>
                      </div>

                    </div>

                  <?php endforeach; ?>
                </div>
              </div>
              <div class="col-3">
                <ul class="tooltips-list">
                  <?php for($i = 1; $i <= 12; $i++): ?>
                    <?php
                      $layerName = 'layer_' . $i . '_name';
                      $layerNameContent = get_field($layerName, $current_post_id);
                    ?>
                    <?php if( $layerNameContent ): ?>
                      <li data-itemnumber="<?php echo $i; ?>"><?php echo $layerNameContent ?></li>
                    <?php endif; ?>
                  <?php endfor; ?>
                </ul>
              </div>
              <div class="wall-name text-right">
                <span class="term"><?php echo $current_term_name ?></span>
                <span class="wall"><?php echo get_field('walls_name', $current_post_id); ?></span>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="text-content">
                  <?php echo get_post_field('post_content', $current_post_id); ?>
                </div>
              </div>
            </div>
    			</div>

        </div>
      </div>
    </div>
  </div>

  <?php
  get_footer();
