<?php 

$posts = array(
  'numberposts' => 6,
  'offset' => 0,
  'category' => 0,
  'orderby' => 'post_date',
  'order' => 'desc',
  'include' => '',
	'exclude' => $post->ID,
	'meta_key' => '',
	'meta_value' =>'',
	'post_type' => 'post',
	'post_status' => 'draft, publish, future, pending, private',
	'suppress_filters' => true
);

$recent_posts = wp_get_recent_posts($posts);

?>

<div class="sidebar-wrapper last_post_sidebar os-animation" data-os-animation="fadeInRight" data-os-animation-delay="0.3s" >
    <div class="row">

      <div class="col">
        <h2>Read more articles</h2>
      </div>

      <?php foreach($recent_posts as $post): ?>
        <div class="col-12">
          <a href="<?php echo get_permalink($post['ID']) ?>">
            <div class="post_item_date">
              <span class="icon icon-calendar"></span>
                <span>
                    <?php echo get_the_date( 'd.m.Y', $post['ID']) ?>
                </span>
            </div>
            <div class="row">
              <div class="col-9">
                  <?php echo get_excerpt_by_id($post['ID']);; ?>
              </div>
              <div class="col-3">
                <button class="btn btn-default">READ</button>
              </div>
            </div>

          </a>

        </div>
      <?php endforeach; ?>

    </div>
</div>