<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<div class="wrapper-404 d-flex align-items-center">
	<div class="container">
		<div class="row">
			<div class="col">
				<h2 class="main_404_heading">404</h2>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<h3 class="second_404_heading">Page not found</h3>
			</div>
		</div>
		
	</div>
</div>

<?php get_footer(); ?>
