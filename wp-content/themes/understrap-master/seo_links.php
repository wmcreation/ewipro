<?php
  $args_cat = array(
    'post_type' => 'seo_links',
    'posts_per_page' => 100,
    'taxonomy' => 'seo_category'
  );
  $cat = get_categories($args_cat);
  $taxonomies = array(
    'seo_category',
  );
  $terms = get_terms($taxonomies);

  $i = 0;
?>

<div id="wrapper-seo-links">
  <div class="container">

    <div class="row">

      <div class="d-block d-sm-flex d-lg-block align-items-center justify-content-between col-sm-12 col-lg-3 left-side order-12 order-lg-1">
        <div class="logo">
          <img src="<?php echo get_template_directory_uri().'/assets/img/ewipro-logo.svg' ?>" alt="logo">
        </div>
        <p class="company-address">
          UNIT1, Kingston Business Centre <br> Fullers Way 1, Chessington <br> Surrey KT9 1DQ
        </p>
        <div class="social">
          <h5 class="social-heading">Check out our channels</h5>
          <div class="social-links d-flex d-lg-block justify-content-center">
            <a href="https://www.facebook.com/EWIProInsulation/" class="social-link-fb"><i class="icon-fb"></i></a>
            <a href="https://www.linkedin.com/company/ewipro/" class="social-link-in"><i class="icon-in"></i></a>
            <a href="https://twitter.com/EWI_Pro" class="social-link-tw"><i class="icon-tw"></i></a>
          </div>
        </div>
      </div>

      <?php foreach ($terms as $key => $value): ?>
      <div class="col-6 col-md-3 col-lg-2 order-1 order-lg-12 <?php if($i == 0) echo 'offset-lg-1'; ?> os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.6s">
          <div class="seo-category <?php if($i == 2 || $i == 3) echo 'no-padding-top' ?>">
            <?php
              $args_post = array(
                  'post_type'   => 'seo_links',
                  'posts_per_page' => 100,
                  'tax_query' => array(
                      array(
                          'taxonomy' => 'seo_category',
                          'field' => $value->term_id,
                          'terms' =>$value->name,
                      )
                  ),
              );
              $temp_posts = get_posts( $args_post );
              $posts = array_reverse($temp_posts);
              
            ?>
            <h5 class="category-name">
              <?php echo $value->name ?>
            </h5>
            <ul>
                <?php foreach ($posts as $k2 => $v2):
                  $meta_value_1 = get_field('address_url', $v2->ID); ?>
                  <?php if(get_the_terms($v2->ID, 'seo_category')[0]->name == $value->name): ?>
                    <li>
                      <a href="<?php echo $meta_value_1 ?>">
                          <?php echo $v2->post_title ?>
                      </a>
                    </li>
                  <?php endif; ?>
                <?php endforeach; ?>
            </ul>
          </div>
      </div>
      <?php $i++ ?>
      <?php endforeach; ?>

    </div>

  </div>

  <div class="copyright-box">
    <div class="container">
      <div class="row no-gutters">
        <div class="col-12 col-lg-8 offset-lg-4">
          <p class="copyright-text">Copyright © 2012-<?php echo date('Y'); ?> EWIPRO LTD. All rights reserved</p>
        </div>
      </div>   
    </div>
  </div>
</div>
