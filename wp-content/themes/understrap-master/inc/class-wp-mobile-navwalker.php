<?php
/**
 * WP Bootstrap Navwalker
 *
 * @package WP-Bootstrap-Navwalker
 */

/*
 * Class Name: WP_Bootstrap_Navwalker
 * Plugin Name: WP Bootstrap Navwalker
 * Plugin URI:  https://github.com/wp-bootstrap/wp-bootstrap-navwalker
 * Description: A custom WordPress nav walker class to implement the Bootstrap 4 navigation style in a custom theme using the WordPress built in menu manager.
 * Author: Edward McIntyre - @twittem, WP Bootstrap, William Patton - @pattonwebz
 * Version: 4.1.0
 * Author URI: https://github.com/wp-bootstrap
 * GitHub Plugin URI: https://github.com/wp-bootstrap/wp-bootstrap-navwalker
 * GitHub Branch: master
 * License: GPL-3.0+
 * License URI: http://www.gnu.org/licenses/gpl-3.0.txt
*/

/**
 * Expected Markup
 *
 * <div id="mp-pusher" class="mp-pusher">
 *      <a href="#" id="trigger" class="menu-trigger">Open/Close Menu</a>
 *      <nav id="mp-menu" class="mp-menu">
 *           <div class="mp-level">
 *               <ul>
 *                   <li>
 *                       <a href="#">Devices</a>
 *                       <div class="mp-level">
 *                           <ul>
 *                               <li>
 *                                   <a href="#">Test</a>
 *                               </li>
 *                           </ul>
 *                       </div>
 *                   </li>
 *               </ul>
 *           </div>
 *      </nav>
 *   </div>
 *
 */



/* Check if Class Exists. */
if ( ! class_exists( 'WP_Mobile_Navwalker' ) ) {
    /**
     * WP_Bootstrap_Navwalker class.
     *
     * @extends Walker_Nav_Menu
     */
    class WP_Mobile_Navwalker extends Walker_Nav_Menu {

        private $curItem;

        public function start_lvl( &$output, $depth = 0, $args = array() ) {



            if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
                $t = '';
                $n = '';
            } else {
                $t = "\t";
                $n = "\n";
            }
            $indent = str_repeat( $t, $depth );

            // Default class.
            $classes = array( 'sub-menu' );


            $class_names = join( ' ', apply_filters( 'nav_menu_submenu_css_class', $classes, $args, $depth ) );
            $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';



            $output .= "{$n}{$indent}<ul>{$n}";
        }


        public function end_lvl( &$output, $depth = 0, $args = array() ) {


            if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
                $t = '';
                $n = '';
            } else {
                $t = "\t";
                $n = "\n";
            }
            $indent = str_repeat( $t, $depth );
            $output .= "$indent</ul>{$n}";
        }


        public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

            $this->curItem = $item;

            if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
                $t = '';
                $n = '';
            } else {
                $t = "\t";
                $n = "\n";
            }
            $indent = ( $depth ) ? str_repeat( $t, $depth ) : '';

            $classes = empty( $item->classes ) ? array() : (array) $item->classes;
            $classes[] = 'menu-item-' . $item->ID;


            $args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );


            $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
            $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';


            $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
            $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

            $output .= $indent . '<li>';

            $atts = array();
            $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
            $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
            $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
            $atts['href']   = ! empty( $item->url )        ? $item->url        : '';


            $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

            $attributes = '';
            foreach ( $atts as $attr => $value ) {
                if ( ! empty( $value ) ) {
                    $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                    $attributes .= ' ' . $attr . '="' . $value . '"';
                }
            }

            /** This filter is documented in wp-includes/post-template.php */
            $title = apply_filters( 'the_title', $item->title, $item->ID );


            $title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );

            $icon_text = '';

            if($depth == 0)
                $icon_text = 'icon_right';

            $item_output = $args->before;

            if($args->walker->has_children)
            {
                $item_output .= '<span>';
            }else{
                $item_output .= '<a'. $attributes .'>';
            }





            $item_output .= $args->link_before . $title . $args->link_after;

            if($args->walker->has_children)
            {
                $item_output .= '</span>';
            }else{
                $item_output .= '</a>';
            }


            $item_output .= $args->after;


            $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
        }


        public function end_el( &$output, $item, $depth = 0, $args = array() ) {
            if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
                $t = '';
                $n = '';
            } else {
                $t = "\t";
                $n = "\n";
            }
            $output .= "</li>{$n}";
        }
    }
}
