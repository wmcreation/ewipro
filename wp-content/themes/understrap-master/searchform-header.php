<?php
/**
 * The template for displaying search forms in Underscores.me
 *
 * @package understrap
 */

?>

<form method="get" id="searchform-header" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search" class="order-0 order-lg-1">

	<div class="input-group">
		<input class="field form-control" id="s" name="s" type="text" placeholder="Search" value="<?php the_search_query(); ?>">
		<button type="submit" class="submit btn btn-success" id="searchsubmit" name="submit">
			<i class="icon icon-search"></i>
		</button>
	</div>

</form>

<button class="search-icon btn btn-success" type="button" name="button">
	<i class="icon icon-search"></i>
</button>
