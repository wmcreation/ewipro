<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php //get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<?php get_template_part(  'seo_links' ); ?>


</div>	<!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>

