<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

get_header();
$container   = get_theme_mod( 'understrap_container_type' );

$current_post_id = $post->ID;

$current_term = wp_get_post_terms($post->ID, 'products_tax')[0]->term_id;
$current_term_name = wp_get_post_terms($post->ID, 'products_tax')[0]->name;

?>

<div class="product_header">

  <div class="container">
    <div class="row">
      <div class="col-lg-2 align-self-center">
        <h4 class="os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">CATEGORIES</h4>
      </div>
      <div class="col-lg-9 offset-lg-1 os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
          <?php
          $categories = get_categories(array(
              'taxonomy'  => 'products_tax',
              'hide_empty' => false
          ));
          foreach($categories as $category): ?>
            <a href="<?php echo get_category_link($category->term_id) ?>" class="<?php if($current_term_name == $category->name) echo 'active' ?>">
                <?php echo $category->name ?>
            </a>
          <?php endforeach ?>
      </div>
    </div>
  </div>
</div>
<div class="container product_container">
  <div class="row">
    <div class="col-xxl-2 col-lg-3 d-none d-lg-block left_product_list_col">
      <div class="left_product_list os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s"">
      <div class="offset_15" ></div>
      <h3 class="os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.45s">CHOOSE PRODUCTS</h3>
      <ul class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.45s">
          <?php

          $queryProduct = new WP_Query( array(
              'post_type' => 'ewi_product',          // name of post type.
              'tax_query' => array(
                  array(
                      'taxonomy' => 'products_tax',   // taxonomy name
                      'field' => $current_term_name,           // term_id, slug or name
                      'terms' => $current_term,                  // term id, term slug or term name
                  )
              )
          ) );

          while ( $queryProduct->have_posts() ) : $queryProduct->the_post(); ?>

            <li>
              <a href="<?php echo the_permalink() ?>" class="<?php if($post->ID == $current_post_id) echo 'active'?>"><?php echo the_title() ?></a>
            </li>

          <?php endwhile;

          wp_reset_query();

          ?>
      </ul>
    </div>
  </div>
  <div class="order-1 order-md-2 col-lg-4 col-md-5 offset-xxl-1">
    <div class="product_image os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.6s"">
      <?php if( !empty( get_the_post_thumbnail_url($post->ID) )): ?>
        <a href="<?php echo get_the_post_thumbnail_url($post->ID) ?>" data-fancybox>
          <img src="<?php echo get_the_post_thumbnail_url($post->ID,'product-thumb') ?>" />
          <span href="<?php echo get_the_post_thumbnail_url($post->ID) ?>" class="icon icon-magnifier-plus"></span>
        </a>
      <?php endif; ?>
  </div>
    <?php
    $file_1 = get_field('plik_1');
    $file_2 = get_field('plik_2')
    ?>
    <?php if($file_1 || $file_2): ?>
      <div class="product_data_sheets">
        <h5 class="os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">Downloads</h5>
        <div class="download_items os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
            <?php if($file_1): ?>
              <a href="<?php echo $file_1['url'] ?>" data-fancybox class="download_item">
                <p>Safety<br />
                  data sheet</p>
                <span class="icon icon-download"></span>
              </a>
            <?php endif; ?>
            <?php if($file_2): ?>
              <a href="<?php echo $file_2['url'] ?>" data-fancybox class="download_item">
                <p>PRODUCT<br />
                  data sheet</p>
                <span class="icon icon-download"></span>
              </a>
            <?php endif; ?>
        </div>
      </div>
    <?php endif; ?>
</div>
<div class="order-2 order-md-1 col-lg-5 col-md-7">
  <div class="product_content os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.45s">
      <?php echo the_content() ?>
  </div>
  <div class="product_share">
    <div class="row">
      <div class="col-auto align-self-center">
        <h5 class="os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">Share this product</h5>
      </div>
      <div class="col-auto os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.3s">
        <a href="http://www.facebook.com/sharer.php?u=<?php esc_url(the_permalink());?>" target="_blank" class="social-link-fb"><i class="icon icon-fb"></i></a>
        <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&source=<?php echo esc_url(home_url()); ?>"  count-layout="none" target="_blank" class="social-link-in"><i class="icon icon-in"></i></a>
        <a href="http://twitter.com/home?status=Reading: <?php esc_url(the_permalink()); ?>" title="<?php esc_html_e('','ewipro'); ?>" target="_blank" class="social-link-tw"><i class="icon icon-tw"></i></a>
      </div>
    </div>
  </div>
</div>

</div>
</div>

<div class="related_product">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.3s">Other <b><?php echo $current_term_name ?></b></h3>
      </div>
    </div>
    <div class="row position-relative">
      <div class="related-product-next"><span class="icon icon-arrow-right"></span></div>
      <div class="related-product-prev"><span class="icon icon-arrow-left"></span></div>
      <div class="swiper-container related-product-slider">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <?php

            $queryRelatedProduct = new WP_Query( array(
                'post__not_in' => array($post->ID),
                'post_type' => 'ewi_product',          // name of post type.
                'tax_query' => array(
                    array(
                        'taxonomy' => 'products_tax',   // taxonomy name
                        'field' => $current_term_name,           // term_id, slug or name
                        'terms' => $current_term,                  // term id, term slug or term name
                    )
                )
            ) );

            while ( $queryRelatedProduct->have_posts() ) : $queryRelatedProduct->the_post(); ?>

              <div class="swiper-slide">
                <a href="<?php echo the_permalink() ?>" class="related_item">
                  <img data-src="<?php echo get_the_post_thumbnail_url($post->ID,'product-thumb') ?>" class="swiper-lazy" />
                    <?php echo the_title() ?>
                </a>
              </div>

            <?php endwhile;

            wp_reset_query();

            ?>
        </div>
      </div>
    </div>
  </div>
</div>



<?php get_footer(); ?>
