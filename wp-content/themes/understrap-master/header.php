<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>

  <noscript>
    <style>
      .os-animation{
        opacity: 1 !important;
      }
    </style>
  </noscript>

</head>

<body <?php body_class(); ?> data-spy="scroll" data-target=".scrollspy_menu" data-offset="115">
<div id="page">
    <?php wp_mobile_nav_menu(
        array(
            'theme_location'  => 'primary',
            'container'       => 'nav',
            'container_id'    => 'menu',
            'menu_class'      => '',
            'fallback_cb'     => '',
            'menu_id'         => ' ',
            'depth'           => 2,
            'walker'          => new WP_Mobile_Navwalker(),
        )
    ); ?>

	<!-- ******************* The Navbar Area ******************* -->
	<div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">
    <div class="header-support">
      <div class="container">
        <div class="row">
          <div class="col-12 text-right">
             <p>Technical support <a href="tel:08001337072">08001337072</a> <a href="mailto: info@ewipro.com">info@ewipro.com</a></p>
          </div>
        </div>
      </div>
    </div>

		<a class="skip-link screen-reader-text sr-only" href="#content"><?php esc_html_e( 'Skip to content', 'understrap' ); ?></a>

		<nav class="navbar navbar-expand-lg navbar-dark Fixed fixed-top " data-os-animation="fadeInDown" data-os-animation-delay="0.3s">
			<div class="menu-wrapper">

					<!-- Your site title as branding in the menu -->
					<?php if ( ! has_custom_logo() ): ?>

						<?php if ( is_front_page() && is_home() ) : ?>

							<h1 class="navbar-brand mb-0"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a></h1>

						<?php else : ?>

							<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>

						<?php endif; ?>

					<?php  else: ?>
            <a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url">
                <img src="/wp-content/themes/understrap-master/assets/img/ewipro-logo.svg" alt="">
            </a>
				  <?php	endif; ?><!-- end custom logo -->


				<!-- The WordPress Menu for resolution >= 992px goes here -->
				<?php wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'collapse navbar-collapse order-last order-lg-0',
						'container_id'    => 'navbarNavDropdown',
						'menu_class'      => 'navbar-nav ml-auto',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'depth'           => 2,
						'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
					)
				); ?>




				<!-- The SearchForm goes here -->
				<?php // include('wp-content/themes/understrap-master/searchform-header.php'); ?>

				<!-- Toggler -->
				<a class="navbar-toggler mm-open-menu-btn" id="trigger" aria-expanded="false">
						<span></span>
						<span></span>
						<span></span>
				</a>

			</div><!-- .menu-wrapper -->

		</nav><!-- .site-navigation -->

	</div><!-- #wrapper-navbar end -->
