(function($) {
  function mainNav() {
    var scroll = $(window).scrollTop(),
      offsetTop = $(".header-support").height();

    if (scroll > offsetTop) {
      $(".fixed-top").addClass("topNull");
    } else {
      $(".fixed-top").removeClass("topNull");
    }
  }

  mainNav();

  var swiperRelatedProduct = new Swiper(".related-product-slider", {
    preloadImages: false,
    slidesPerView: 5,
    loopedSlides: 5,
    speed: 1000,
    spaceBetween: 30,
    //simulateTouch: false,
    //loop: true,
    lazy: {
      loadPrevNext: true
    },
    navigation: {
      nextEl: ".related-product-next",
      prevEl: ".related-product-prev"
    },
    breakpoints: {
      991: {
        slidesPerView: 3,
        spaceBetween: 20
      },
      567: {
        slidesPerView: 1
      }
    }
  });

  var swiperHomeWallsSlider = new Swiper(".home_walls_slider", {
    preloadImages: false,
    slidesPerView: 1,
    effect: "fade",
    fadeEffect: {
      crossFade: true
    },
    speed: 800,
    spaceBetween: 10,
    loop: true,
    autoplay: {
      delay: 3000
    },
    lazy: {
      loadPrevNext: true,
      loadPrevNextAmount: 1
    },
    navigation: {
      nextEl: ".home-walls-slider-next",
      prevEl: ".home-walls-slider-prev"
    }
  });

  var swiper = new Swiper(".home_start_slider", {
    preloadImages: false,
    slidesPerView: 1,
    effect: "fade",
    speed: 1000,
    spaceBetween: 10,
    autoplay: {
      delay: 3000
    },
    loop: true,
    pagination: {
      el: ".home-swiper-pagination",
      clickable: true,
      renderBullet: function(index, className) {
        if (index < 10) {
          return (
            '<span class="' +
            className +
            '"> <span class="number">.0' +
            (index + 1) +
            "</span></span>"
          );
        } else {
          return (
            '<span class="' +
            className +
            '"><span class="number">' +
            (index + 1) +
            "</span></span>"
          );
        }
      }
    }
  });

  var homeProductQuick = new Swiper(".products-slider", {
    preloadImages: false,
    observer: true,
    observeParents: true,
    slidesPerView: 5,
    centeredSlides: true,
    //loop: true,
    speed: 800,
    spaceBetween: 50,
    initialSlide: 2,
    //simulateTouch: false,
    navigation: {
      nextEl: ".home-quick-product-next",
      prevEl: ".home-quick-product-prev"
    },
    lazy: {
      loadPrevNext: true,
      loadPrevNextAmount: 3
    },
    on: {
      init: function() {
        setTimeout(function() {
          $(".products-slider").addClass("swiper-is-loaded");
        }, 500);
      }
    },
    breakpoints: {
      991: {
        slidesPerView: 3,
        spaceBetween: 20
      },
      567: {
        slidesPerView: 1,
        spaceBetween: 20
      }
    }
  });

  var homeProductQuickName = new Swiper(".products-names-slider", {
    //direction: 'vertical',
    observeParents: true,
    observer: true,
    effect: "fade",
    slidesPerView: 5,
    //loop: true,
    speed: 1000,
    initialSlide: 2
    //simulateTouch: false,
  });

  if (homeProductQuick.length > 1) {
    for (var i = 0; i < homeProductQuick.length; i++) {
      homeProductQuick[i].controller.control = homeProductQuickName[i];
      homeProductQuickName[i].controller.control = homeProductQuick[i];
    }
  } else if (homeProductQuick.length == 1) {
    homeProductQuick.controller.control = homeProductQuickName;
    homeProductQuickName.controller.control = homeProductQuick;
  }

  function playIfOnScreen($video) {
    if ($("#" + $video).length) {
      var video = $("#" + $video),
        video_height = $("#" + $video).outerHeight(),
        current_scroll = $(window).scrollTop();

      if ($(".section-video:visible")) {
        if ( video.offset().top < current_scroll + video_height && current_scroll < video.offset().top + video_height - video_height ) {
          $("#" + $video).addClass("is-visible");
          document.getElementById($video).play();
        } else {
          document.getElementById($video).pause();
        }
      }
    }
  }

  function onScrollInit(items, trigger) {
    items.each(function() {
      var osElement = $(this),
        osAnimationClass = osElement.attr("data-os-animation"),
        osAnimationDelay = osElement.attr("data-os-animation-delay");

      osElement.css({
        "-webkit-animation-delay": osAnimationDelay,
        "-moz-animation-delay": osAnimationDelay,
        "animation-delay": osAnimationDelay
      });

      var osTrigger = trigger ? trigger : osElement;

      osTrigger.waypoint(
        function() {
          osElement.addClass("animated").addClass(osAnimationClass);
        },
        {
          triggerOnce: true,
          offset: "85%"
        }
      );
    });
  }
  onScrollInit($(".os-animation"));

  $(window).scroll(function() {
    mainNav();
    playIfOnScreen("video-about-home");
    playIfOnScreen("video-about");
    playIfOnScreen("video-training-header");
  });

  $("#wrapper-walls .content-item").click(function(e) {
    var hoverImage = $("#" + $($(this).data("id")).selector),
      currentSrc = hoverImage.data("src");
    $("#wrapper-walls .content-item").removeClass("active");
    $(this).addClass("active");
    if (!hoverImage.hasClass("active")) {
      $("#wrapper-walls .middle-content").addClass("image-loading");

      hoverImage.attr("src", currentSrc);

      hoverImage
        .on("load", function() {
          console.log("image loaded correctly");

          setTimeout(function() {
            $("#wrapper-walls .middle-content img").removeClass("active");

            hoverImage.addClass("active");
          }, 500);

          setTimeout(function() {
            $("#wrapper-walls .middle-content").removeClass("image-loading");
          }, 1000);
        })
        .on("error", function() {
          console.log("error loading image");
        });
    }
  });

  $("#main-menu").append('<li class="bottom-bar"></li>');

  $("#main-menu .nav-item").on("mouseout", function() {
    var el = $("#main-menu .nav-item.active");

    $(".bottom-bar")
      .css("left", el.position().left)
      .width(el.outerWidth());
  });

  $("#main-menu .nav-item").on("mouseover", function() {
    var el = $(this);

    $(".bottom-bar")
      .css("left", el.position().left)
      .width(el.outerWidth());
  });

  $("#main-menu .nav-item.active").trigger("mouseout");

  ///PRODUCT SCRIPTS

  $(".left_product_list").stick_in_parent({
    offset_top: 150
  });

  //DOWNLOAD PAGE

  $(".download_row [data-id]").on("click", function() {
    var closestRow = $(this).closest(".download_row"),
      clickedDataId = $(this).data("id");

    closestRow.find(".download_right_item").removeClass("active");
    closestRow.find(".download_center li").removeClass("active");

    $(this).addClass("active");

    closestRow.find("#" + clickedDataId).addClass("active");
  });

  $(function() {
    $(".lazy").lazy({
      threshold: 0,
      afterLoad: function(element) {
        element.addClass("loaded");
      }
    });
  });

  // // Checking resolution and changing drop menu style
  // var menuItem = $('#main-menu .menu-item');
  //
  // $(window).on('load resize', function () {
  //   if ($(window).width() < 992) {
  //     menuItem.removeClass('dropdown').addClass('dropright');
  //   } else {
  //     menuItem.removeClass('dropright').addClass('dropdown');
  //   }
  // });

  // Checking wp-adminbar height

  $(window).on("load resize", function() {
    setTimeout(function() {
      var wpAdminBarHeight = $("#wpadminbar").outerHeight();
      if (wpAdminBarHeight === 32) {
        $("body")
          .removeClass("h-46")
          .addClass("h-" + wpAdminBarHeight);
      } else if (wpAdminBarHeight === 46) {
        $("body")
          .removeClass("h-32")
          .addClass("h-" + wpAdminBarHeight);
      }
    }, 150);
  });

  // SearchBox SlideIn < 768px vw

  $(".search-icon").on("click", function() {
    $("#searchform-header").toggleClass("active");
  });

  // Prevent collapsing Bootstrap collapse (one of them everytime should be visible)
  $('a[data-toggle="collapse"]').click(function(e) {
    var target = $(this).data("target");
    if ($(target).hasClass("show")) {
      e.stopPropagation();
      e.preventDefault();
    }
  });

  var previousTarget = new Array();
  var count = 0;

  // Wall Build Up tooltips list clicking
  $("li[data-itemnumber]").click(function() {
    var target = $(this).data("itemnumber");
    var notCurrentTarget = $("[data-itemnumber]").not("[data-itemnumber='" + target + "']");
    var tooltip = $("[data-current='" + target + "']");
    var notCurrentTooltip = $("[data-current]").not("[data-current='" + target + "']");

    previousTarget.unshift(target);

    tooltip.toggleClass("active");
    $(this).toggleClass("active");

    notCurrentTarget.removeClass("active");
    notCurrentTooltip.removeClass("active");

  });

  $("div.image-point").on('mouseenter', function() {
    var target = $(this).data('current');
    notCurrentTarget = $("[data-current]").not("[data-current='" + target + "']");
    if( $(this).not('.active') ) {
      notCurrentTarget.removeClass("active");
      $("li[data-itemnumber]").not("[data-itemnumber='" + target + "']").removeClass("active");
    }
  });

})(jQuery);

jQuery(function() {
  jQuery("a[href*=#]:not([href=#])").click(function() {
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = jQuery(this.hash);
      target = target.length
        ? target
        : jQuery("[name=" + this.hash.slice(1) + "]");
      if (target.length) {
        jQuery("html,body").animate(
          {
            scrollTop:
              target.offset().top -
              jQuery("#wrapper-navbar .navbar").outerHeight()
          },
          1000
        );
        return false;
      }
    }
  });
});

jQuery(document).ready(function($) {
  var mmenu = $("#menu")
    .mmenu({})
    .data("mmenu");

  $("#trigger").click(function(ev) {
    ev.preventDefault();
    mmenu.open();
  });
});


